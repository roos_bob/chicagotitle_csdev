function ShellParse() {
    var that = new BatchParse;

    that.parseData = function() {
        $j("#popupLoading").show();
        $j("#closingDate").hide();
        that.addHeaders();
        that.addRecords();
        that.getValidationData()
        console.log("Synchronous parse results:", inputData);
    }

    that.quickParse = function() {

        $j("#closingDate").hide();
        $j("#expirationDate").show();
        that.parseData();
        $j("#submit-parse").show();
		$j("#popupLoading").hide();
        
    }    

    that.addHeaders = function() {
        var i = 1;
        var columns = [];
        columns.push( {name: "Problems", label: "Problems", inputColumn: 0, visible: true} );
        columns.push( {name: "Verified", label: "Verified", inputColumn: 0, visible: true} );

        columns.push( {name:  "No", label: "No.", inputColumn: i++, visible: true} );
        columns.push( {name:  "SLC_CTR", label: "SLC CTR", inputColumn: i++, visible: true} );
        columns.push( {name:  "Cash_Check", label: "Cash Check", inputColumn: i++, visible: true} );
        columns.push( {name:  "Credit_Card", label: "Credit Card", inputColumn: i++, visible: true} );
        columns.push( {name:  "Sale_Date", label: "Sale Date", inputColumn: i++, visible: true} );
        columns.push( {name:  "Reloads", label: "Reloads", inputColumn: i++, visible: true} );
        columns.push( {name:  "Equiant_loan_number", label: "Equiant loan number", inputColumn: i++, visible: true} );
        columns.push( {name:  "Contract_No", label: "Contract #", inputColumn: i++, visible: true} );
        columns.push( {name:  "Name1", label: "Name1", inputColumn: i++, visible: true} );
        columns.push( {name:  "Name2", label: "Name2", inputColumn: i++, visible: true} );
        columns.push( {name:  "Conversion_Points", label: "Conversion Points", inputColumn: i++, visible: true} );
        columns.push( {name:  "New_Points", label: "New Points", inputColumn: i++, visible: true} );
        columns.push( {name:  "Price_of_New_Points", label: "Price of New Points", inputColumn: i++, visible: true} );
        columns.push( {name:  "Conversion_Fee", label: "Conversion Fee", inputColumn: i++, visible: true} );
        columns.push( {name:  "Club_Fee", label: "Club Fee", inputColumn: i++, visible: true} );
        columns.push( {name:  "Down_Payment", label: "Down Payment", inputColumn: i++, visible: true} );
        columns.push( {name:  "New_Lending", label: "New Lending", inputColumn: i++, visible: true} );
        columns.push( {name:  "New_Loan_Balance", label: "New Loan Balance", inputColumn: i++, visible: true} );
        columns.push( {name:  "Equity", label: "Equity", inputColumn: i++, visible: true} );
        columns.push( {name:  "Contract_Value", label: "Contract Value", inputColumn: i++, visible: true} );
        columns.push( {name:  "Reserve_Fee", label: "Reserve Fee", inputColumn: i++, visible: true} );
        columns.push( {name:  "Escrow_Cost", label: "Escrow Cost", inputColumn: i++, visible: true} );
        columns.push( {name:  "Credit_Score", label: "Credit Score", inputColumn: i++, visible: true} );
        columns.push( {name:  "Address", label: "Address", inputColumn: i++, visible: true} );
        columns.push( {name:  "City", label: "City", inputColumn: i++, visible: true} );
        columns.push( {name:  "State", label: "State", inputColumn: i++, visible: true} );
        columns.push( {name:  "Zip_Code", label: "Zip Code", inputColumn: i++, visible: true} );
        columns.push( {name:  "Country", label: "Country", inputColumn: i++, visible: true} );
        columns.push( {name:  "Member_Type", label: "Member Type", inputColumn: i++, visible: true} );
        columns.push( {name:  "Membership_Type", label: "Membership Type", inputColumn: i++, visible: true} );
        columns.push( {name:  "Member_ID", label: "Member ID", inputColumn: i++, visible: true} );

        columns.push( {name:  "ClosingDate", label: "Closing Date", inputColumn: 0, visible: true} );
        columns.push( {name:  "BatchNumber", label: "Batch Number", inputColumn: 0, visible: true} );
        columns.push( {name:  "Region", label: "Region", inputColumn: 0, visible: true} );
        columns.push( {name:  "ResortCode", label: "Resort Code", inputColumn: 0, visible: true} );
        columns.push( {name:  "Lender", label: "Lender", inputColumn: 0, visible: true} );
        columns.push( {name:  "FullLender", label: "Full Lender", inputColumn: 0, visible: true} );
        columns.push( {name:  "ExpirationDate", label: "Expiration Date", inputColumn: 0, visible: true} );

        columns.push( {name:  "Name1Salutation", label: "Name 1 Salutation", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1First", label: "Name 1 First", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Middle", label: "Name 1 Middle", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Last", label: "Name 1 Last", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Suffix", label: "Name 1 Suffix", inputColumn: 0, visible: true} );

        columns.push( {name:  "Name2Salutation", label: "Name 2 Salutation", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2First", label: "Name 2 First", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Middle", label: "Name 2 Middle", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Last", label: "Name 2 Last", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Suffix", label: "Name 2 Suffix", inputColumn: 0, visible: true} );

        columns.push( {name: "LenderId", label: "Lender Id", inputColumn: 0, visible: true} );
        columns.push( {name: "ResortId", label: "Resort Id", inputColumn: 0, visible: true} );
        columns.push( {name: "ExpirationDateId", label: "Expiration Date Id", inputColumn: 0, visible: true} );
        columns.push( {name: "MembershipTypeId", label: "Membership Type Id", inputColumn: 0, visible: true} );
        columns.push( {name: "MemberTypeId", label: "Member Type Id", inputColumn: 0, visible: true} );
        columns.push( {name: "TransactionTypeId", label: "Transaction Type Id", inputColumn: 0, visible: true} );

        inputData.headers = columns;

    }

    that.addRecords = function() {
        inputData.records = [];
        var closingDateRaw = inputData.data[0][8];
        var batchNumberRaw = inputData.data[1][8];
        var resortCodeRaw = inputData.data[2][8];

        var closingDate = closingDateRaw.substring(24, closingDateRaw.length) ;
        var batchNumber = replaceAll(closingDate, "/", "") + batchNumberRaw.substring(22, batchNumberRaw.length);
        var resortCode = resortCodeRaw.substr(-2);
        var regionCode = resortCodeRaw.substr(-5).substr(0,2);

        console.log("resort", resortCodeRaw);
        var defaultExpirationDate = $j("#ExpirationPickList option:selected").text();
        if (defaultExpirationDate == 'Select Date') defaultExpirationDate = '';
        inputData.data.forEach(function(item) {
            if (item.length < 10) {return;} // intended to remove a trailing new line row
            var rowData = {};
            rowData.isError = false;
            var i = 0;
            rowData.No = {value: item[i++].trim(), valid: true};
            rowData.SLC_CTR = {value: item[i++].trim(), valid: true}; // This should be a resort code, but is not used?
            rowData.Cash_Check = {value: item[i++].trim(), valid: true};
            rowData.Credit_Card = {value: item[i++].trim(), valid: true};
            rowData.Sale_Date = {value: item[i++].trim(), valid: true};
            rowData.Reloads = {value: item[i++].trim(), valid: true};
            rowData.Equiant_loan_number = {value: item[i++].trim(), valid: true};
            rowData.Contract_No = {value: item[i++].trim(), valid: true};

            rowData.Name1 = {value: item[i++].trim(), valid: true};
            rowData.Name1First = {value: '', valid: true};
            rowData.Name1Middle = {value: '', valid: true};
            rowData.Name1Last = {value: '', valid: true};
            rowData.Name1Suffix = {value: '', valid: true};

            rowData.Name2 = {value: item[i++].trim(), valid: true};
            rowData.Name2Salutation = {value: '', valid: true};
            rowData.Name2First = {value: '', valid: true};
            rowData.Name2Middle = {value: '', valid: true};
            rowData.Name2Last = {value: '', valid: true};
            rowData.Name2Suffix = {value: '', valid: true};


            rowData.Conversion_Points = {value: item[i++].trim(), valid: true};
            rowData.New_Points = {value: item[i++].trim(), valid: true};
            rowData.Price_of_New_Points = {value: item[i++].trim(), valid: true};
            rowData.Conversion_Fee = {value: item[i++].trim(), valid: true};
            rowData.Club_Fee = {value: item[i++].trim(), valid: true};
            rowData.Down_Payment = {value: item[i++].trim(), valid: true};
            rowData.New_Lending = {value: item[i++].trim(), valid: true};
            rowData.New_Loan_Balance = {value: item[i++].trim(), valid: true};
            rowData.Equity = {value: item[i++].trim(), valid: true};
            rowData.Contract_Value = {value: item[i++].trim(), valid: true};
            rowData.Reserve_Fee = {value: item[i++].trim(), valid: true};
            rowData.Escrow_Cost = {value: item[i++].trim(), valid: true};
            rowData.Credit_Score = {value: item[i++].trim(), valid: true};
            rowData.Address = {value: item[i++].trim(), valid: true};
            rowData.City = {value: item[i++].trim(), valid: true};
            rowData.State = {value: item[i++].trim(), valid: true};
            rowData.Zip_Code = {value: item[i++].trim(), valid: true};
            rowData.Country = {value: item[i++].trim(), valid: true};
            rowData.Member_Type = {value: item[i++].trim(), valid: true};
            rowData.Membership_Type = {value: item[i++].trim(), valid: true};
            rowData.Member_ID = {value: item[i++].trim(), valid: true};



            rowData.Problems = {value: '', valid: true};
            rowData.Verified = {value: 'false', valid: true};
            rowData.ClosingDate = {value: closingDate, valid: true}; // spreadsheet cell I1 contains closing date
            rowData.BatchNumber = {value: batchNumber, valid: true};
            rowData.ResortCode = {value: resortCode, valid: true};
            if (resortCode == '78' || resortCode == '79') {
                rowData.Lender = {value: 'Wellington Financial/Liberty Bank' , valid: true};
                rowData.FullLender = {value: 'Lender: Wellington Financial/Liberty Bank', valid: true};
            } else {
                rowData.Lender = {value: 'Unknown' , valid: true};
                rowData.FullLender = {value: '', valid: true};

            }
            rowData.Region = {value: regionCode, valid: true};

            rowData.ExpirationDate = {value: '', valid: true};

            rowData.LenderId = {value: '', valid: true};
            rowData.ResortId = {value: '', valid: true};
            rowData.ExpirationDateId = {value: '', valid: true};
            rowData.MembershipTypeId = {value: '', valid: true};
            rowData.MemberTypeId = {value: '', valid: true};
            rowData.TransactionTypeId = {value: '', valid: true};



            // Check membership type: if "Permanent", set value of "ExpirationDate" = ""
            if (rowData.Membership_Type.value == 'Permanent') {
                rowData.ExpirationDate.value = '';
            }
            // Check membership type: if "Standard", set value of "ExpirationDate" = default date
            if (rowData.Membership_Type.value == 'Standard') {
                //TODO:  pull default expiration date from a control?
                rowData.ExpirationDate.value = defaultExpirationDate;
            }

            if (rowData.Name1.value != '') {
                var n1 = parseFullName(rowData.Name1.value);

                rowData.Name1Salutation = {value: n1.title, valid: true};
                rowData.Name1First = {value: n1.first, valid: true};
                rowData.Name1Middle = {value: n1.middle, valid: true};
                rowData.Name1Last = {value: n1.last, valid: true};
                rowData.Name1Suffix = {value: n1.suffix, valid: true};
            }

            if (rowData.Name2.value != '') {
                var n2 = parseFullName(rowData.Name2.value);
                rowData.Name2Salutation = {value: n2.title, valid: true};
                rowData.Name2First = {value: n2.first, valid: true};
                rowData.Name2Middle = {value: n2.middle, valid: true};
                rowData.Name2Last = {value: n2.last, valid: true};
                rowData.Name2Suffix = {value: n2.suffix, valid: true};
            }



            // validate row and add
            if (rowData.No.value != '' && rowData.No.value != 'No.' ) {
                inputData.records.push(rowData);
            }
        });
    }

    that.getValidationData = function() {
        var contractNumberList = [];
        var lenderList = [];
        var resortCodeList = [];
        var expirationDateList = [];
        var memberTypeList = [];
        var membershipTypeList = [];
        var records = inputData.records;


        records.forEach(function (record) {
            contractNumberList.push(record.Contract_No.value);
            lenderList.push(record.Lender.value);
            resortCodeList.push(record.ResortCode.value);
            if(record.ExpirationDate.value != '') {expirationDateList.push( record.ExpirationDate.value );}
            memberTypeList.push(record.Member_Type.value);
            membershipTypeList.push(record.Membership_Type.value);
        });
        // JS Remoting Call to get records out of SF
        BatchPointsController.getShellValidationData(
            contractNumberList.filter(onlyUnique), lenderList.filter(onlyUnique), resortCodeList.filter(onlyUnique), expirationDateList.filter(onlyUnique), memberTypeList.filter(onlyUnique), membershipTypeList.filter(onlyUnique),
            function(result) {
                console.log('Async getShellValidationData result', result);
                result.Contract__c.forEach(function(item) { that.sfContracts[item.Contract_Number__c] = item.Id;});
                result.Lender__c.forEach(function(item) { that.sfLenders[item.Name] = item.Id;});
                result.Expiration_Date__c.forEach(function(item) { that.sfExpirationDate[item.Date_String__c] = item.Id; } );
                result.Member_Type__c.forEach(function(item) { that.sfMemberType[item.Name] = item.Id;});
                result.Membership_Type__c.forEach(function(item) { that.sfMembershipType[item.Name] = item.Id;});
                result.Resort__c.forEach(function(item) { that.sfResort[item.Code__c] = item.Id; inputData.meta.developerId = item.Developer__c; });
                result.Transaction_Type__c.forEach(function(item) { that.sfTransactionType["Issued"] = item.Id; });
                records.forEach(function (record) {
                    if (record.Lender.value != '') record.LenderId.value = that.sfLenders[record.Lender.value];
                    if (record.ExpirationDate.value != '') record.ExpirationDateId.value = that.sfExpirationDate[record.ExpirationDate.value];
                    if (record.Member_Type.value != '') record.MemberTypeId.value = that.sfMemberType[record.Member_Type.value];
                    if (record.Membership_Type.value != '') record.MembershipTypeId.value = that.sfMembershipType[record.Membership_Type.value];
                    if (record.ResortCode.value != '') record.ResortId.value = that.sfResort[record.ResortCode.value];
                    record.TransactionTypeId.value = that.sfTransactionType["Issued"];

                });
                console.log('displayExpirationDatePicklist', result.SelectExpirationDate);
                that.displayExpirationDatePicklist(result.SelectExpirationDate);
                that.validateRecords();
                that.displayRecords();
                $j("#expirationDate, #submit-load, #dataTable").show();
            }
        );

    }

    that.validateRecords = function() {
        inputData.meta.unverified = 0;
        var records = inputData.records;
        records.forEach(function (record) {
            // Contract Number starts with correct region "Determine Database/Region 577"
            var region = record["Region"].value;
            if (record["Contract_No"].value != "" && !record["Contract_No"].value.includes(region)){
                record.Contract_No.valid = false;
                var problemMessage = 'Contract # does not match region.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!record.Membership_Type.value == "Standard" && record.ExpirationDate.value == ''){
                record.ExpirationDate.valid = false;
                var problemMessage = 'Must enter an expiration date';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            // Replace nulls
            // Check Contract Number not blank  (No ContractNumber entered)
            if (record["Contract_No"].value == ""){
                record.Contract_No.valid = false;
                record["Problems"] = setProblemMessage(record, "No Contract # entered");
            }

            // Check Membershipnumber for blank (No Membershipnumber entered)
            if (record.Member_ID.value == ""){
                record.Member_ID.valid = false;
                record.Problems = setProblemMessage(record, "No Member ID entered");
            }
            // Check Points exists (No points entered)
            if (record.New_Points.value == ""){
                record.New_Points.valid = false;
                record.Problems = setProblemMessage(record, "No points entered");
            }
            // Check Points is numeric (Not a point value)
            if (record.New_Points.value != "" && !isNumeric(record.New_Points.value)){
                record.New_Points.valid = false;
                record.Problems = setProblemMessage(record, "Not a point value");
            }

            // Check Conversion Points exists (No points entered (Conversion Points))
            if (record.Conversion_Points.value == ""){
                record.Conversion_Points.valid = false;
                record.Problems = setProblemMessage(record, "No points entered (Conversion Points)");
            }
            // Check Conversion Points is numeric (Not a point value (Conversion Points))
            if (record.Conversion_Points.value != "" && !isNumeric(record.Conversion_Points.value)){
                record.Conversion_Points.valid = false;
                record.Problems = setProblemMessage(record, "Not a point value (Conversion Points)");
            }
            // Make sure both Conversion points and Points are not filled out at the same time (Both Points and Conversion Points cannot both have a value (Change one to Zero))
            if (record.Conversion_Points.value != "0" && record.New_Points.value != "0" && record.Conversion_Points.valid == true && record.New_Points.valid == true){
                var problemMessage = 'Both Points and Conversion Points cannot both have a value (Change one to Zero)';
                record.New_Points.valid = false;
                record.Conversion_Points.valid = false;
                record.Problems = setProblemMessage(record, problemMessage);
            }
            // Check Date Signed is valid date(Not a valid date)
            if (record.Sale_Date.value != "" && !Date.parse(record.Sale_Date.value)){
                record.Sale_Date.valid = false;
                var problemMessage = 'Sale Date not a valid Date';
                record.Problems = setProblemMessage(record, problemMessage);
            }
            // Check Date Signed exists (No date entered)
            if (record.Sale_Date.value == ''){
                record.Sale_Date.valid = false;
                record.Problems = setProblemMessage(record, "No Sale Date entered");
            }
            // Check resort code entered (No resort entered)
            if (record.ResortCode.value == ''){
                record.ResortCode.valid = false;
                record.Problems = setProblemMessage(record, "No Resort Entered");
            }

            // Check member type not blank ( Must enter a Member Type)
            if (record.Member_Type.value == ''){
                record.Member_Type.valid = false;
                record.Problems = setProblemMessage(record, "Must enter a Member Type");
            }

            // Check membership type not blank (Must enter a Membership Type)
            if (record.Membership_Type.value == ''){
                record.Membership_Type.valid = false;
                record.Problems = setProblemMessage(record, "Must enter a Membership Type");
            }

            // Check for owners (Must have at least one owner to continue)
            if (record.Name1.value == ''){
                record.Name1.valid = false;
                record.Problems = setProblemMessage(record, "Must have at least one owner");
            }

            // Check Batch (Must enter a Batch Number)
            if (record.BatchNumber.value == ''){
                record.BatchNumber.valid = false;
                record.Problems = setProblemMessage(record, "Must enter a Batch Number");
            }
            // Check Lender not blank (Lender cant be blank)
            if (record.Lender.value == ''){
                record.Lender.valid = false;
                record.Problems = setProblemMessage(record, "Lender can't be blank");
            }
            // Check Expiration Date not blank (Must enter an Expiration date)
            if (record.Membership_Type.value == 'Standard' && record.ExpirationDate.value == ''){
                record.ExpirationDate.valid = false;
                record.Problems = setProblemMessage(record, "Must enter an Expiration date");
            }




            // Check member type (Member Type doesnt exist in database)
            if (that.sfMemberType[record.Member_Type.value] == null && record.Member_Type.value != '') {
                record.Member_Type.valid = false;
                var problemMessage = 'Member Type doesn\'t exist in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            // Check membership type (Membership Type doesnt exist in database)
            if (that.sfMembershipType[record.Membership_Type.value] == null && record.Membership_Type.value != '') {
                record.Membership_Type.valid = false;
                var problemMessage = 'Membership Type doesn\'t exist in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            // Check Expiration Date  (Expiration Date doesnt exist in database)
            if (that.sfExpirationDate[record.ExpirationDate.value] == null && record.ExpirationDate.value != '') {
                record.ExpirationDate.valid = false;
                var problemMessage = 'Expiration Date doesn\'t exist in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Check contract numbers for duplicates (Duplicate Contract Number)
            // Validate Contract Number is not duplicate
            if (that.sfContracts[record.Contract_No.value] != null && record.Contract_No.value != '' ) {
                record.Contract_No.valid = false;
                var problemMessage = 'Contract Number already exists in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            // Handled elsewhere: Check Membershipnumber for duplicates - set MembershipNumberExists = true if exists

            // Check Lender (Lender doesnt exist in database)
            // Lender validate exists in db
            if (that.sfLenders[record.Lender.value] == null  && record.Lender.value != '') {
                record.Lender.valid = false;
                var problemMessage = 'Lender does not exist in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Check resort code (Resort doesnt exist in database)
            if (that.sfResort[record.ResortCode.value] == null && record.ResortCode.value != '') {
                record.ResortCode.valid = false;
                var problemMessage = 'Resort does not exist in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            record["Verified"] = record["isError"] ? {value: 'false', valid: true} : {value: 'true', valid: true};

            inputData.meta.unverified = record["isError"] ? ++inputData.meta.unverified : inputData.meta.unverified;

        });

    }


    that.loadData = function() {
        if(inputData == null) {return;}

        $j("#popupLoading").show();
        var dataRows = [];
        var rowId = 0;
        var batchType = 'Shell';
        var batchId = 'SHELL|' + Date.parse(new Date());
        console.log('Batch Number:', batchId);
        inputData.records.forEach(function(item) {
            //if(item.isError == false) {
            var rowCounter = ++rowId;
            var bc = new Points_Batch_Closing__c();
            bc.Name = batchId + '|' + ('00000' + rowCounter).substr(-5);
            bc.Import_Batch_Identifier__c = batchId;
            bc.Import_Batch_Row__c = rowCounter;

            bc.No__c = item.No.value.ToNumber();
            bc.RC__c = item.SLC_CTR.value;
            //bc.IC_Number__c = item.SLC_CTR.value;
            bc.Cash_Check__c = item.Cash_Check.value.ToNumber();
            bc.Down_Payment__c = item.Credit_Card.value.ToNumber();
            if (!isNaN(Date.parse(item.Sale_Date.value))) bc.Contract_Date__c = item.Sale_Date.value.ToDate();
            bc.Reloads__c = item.Reloads.value.ToNumber();
            bc.Equiant_loan_number__c = item.Equiant_loan_number.value;
            bc.Contract__c = item.Contract_No.value;
            bc.Name_1__c = item.Name1.value;
            bc.Name_1_Salutation__c = item.Name1Salutation.value;
            bc.Name_1_First__c = item.Name1First.value;
            bc.Name_1_Middle__c = item.Name1Middle.value;
            bc.Name_1_Last__c = item.Name1Last.value;
            bc.Name_1_Suffix__c = item.Name1Suffix.value;
            bc.Name_2__c = item.Name2.value;
            bc.Name_2_Salutation__c = item.Name2Salutation.value;
            bc.Name_2_First__c = item.Name2First.value;
            bc.Name_2_Middle__c = item.Name2Middle.value;
            bc.Name_2_Last__c = item.Name2Last.value;
            bc.Name_2_Suffix__c = item.Name2Suffix.value;
            bc.Convert_Points__c = item.Conversion_Points.value.ToNumber();
            bc.New_Points__c = item.New_Points.value.ToNumber();
            bc.Price_of_New_Points__c = item.Price_of_New_Points.value.ToNumber();
            bc.Conv_Fee__c = item.Conversion_Fee.value.ToNumber();
            bc.Club_Fee__c = item.Club_Fee.value.ToNumber();
            bc.Total_Cash_Received__c = item.Down_Payment.value.ToNumber();
            bc.Amt_Finance__c = item.New_Lending.value.ToNumber();
            bc.New_Loan_Balance__c = item.New_Loan_Balance.value.ToNumber();
            bc.Total_Equity__c = item.Equity.value.ToNumber();
            bc.Purchase_Price__c = item.Contract_Value.value.ToNumber();
            bc.Reserve_Fee__c = item.Reserve_Fee.value.ToNumber();
            bc.CTT_Close_Cost__c = item.Escrow_Cost.value.ToNumber();
            bc.Credit_Score__c = item.Credit_Score.value.ToNumber();
            bc.Owner_Address__c = item.Address.value;
            bc.Owner_City__c = item.City.value;
            bc.Owner_State__c = item.State.value;
            bc.Owner_Postal_Code__c = item.Zip_Code.value;
            bc.Owner_Country__c = item.Country.value;
            bc.Member_Type__c = item.Member_Type.value;
            bc.Membership_Type__c = item.Membership_Type.value;
            bc.Owner__c = item.Member_ID.value;

            bc.Batch__c = item.BatchNumber.value.ToNumber();

            bc.Problems__c = item.Problems.value;
            bc.Verified__c = item.Verified.value == 'true';
            if (!isNaN(Date.parse(item.ClosingDate.value))) bc.Closing_Date__c = item.ClosingDate.value.ToDate();
            bc.Region__c = item.Region.value;
            bc.Resort_Code__c = item.ResortCode.value;
            bc.Lender__c = item.Lender.value;
            if (!isNaN(Date.parse(item.ExpirationDate.value))) bc.Expiration_Date__c = item.ExpirationDate.value.ToDate();

            bc.Lender_Id__c = item.LenderId.value;
            bc.Resort__c = item.ResortId.value;
            bc.Expiration_Date_Id__c = item.ExpirationDateId.value;
            bc.Membership_Type_Id__c = item.MembershipTypeId.value;
            bc.Member_Type_Id__c = item.MemberTypeId.value;
            bc.Transaction_Type_Id__c = item.TransactionTypeId.value;

            dataRows.push(bc);

        });

        BatchPointsController.loadData(
            dataRows, batchType, batchId,inputData.meta.developerId,
            function(result) {
                console.log('Async loadData result: ', result);

                // Update UI with restults information
                var bi = $j('<span />');
                for(var prop in result.errors) {
                    console.log(prop + '=' + result.errors[prop]);
                }
                bi.append('<strong>Records created in Salesforce:</strong>');
                bi.append('<br />');
                bi.append('Membership Number records: ' + result.records['Membership_Number__c']);
                bi.append('<br />');
                bi.append('Contract records: ' + result.records['Contract__c']);
                bi.append('<br />');
                bi.append('Transaction records: ' + result.records['Transaction__c']);
                bi.append('<br />');
                bi.append('Owner records: ' + result.records['Owner__c']);
                bi.append('<br />');
                bi.append('Owner List records: ' + result.records['Owner_List__c']);
                bi.append('<br />');
                bi.append('<br />');
                $j('#insertResult').html(bi);

                var link = $j('<span />');
                link.append('<strong>Link to this batch record:</strong>');
                link.append('<br />');
                var anchor = $j('<a href="/' + result.records['Points_Batch_Header__c'] + '">Batch Record</a>');
                link.append(anchor);
                $j('#batchRecordLink').html(link);
                $j('#submit-load').hide();

                $j("#popupLoading").hide();
            }
        );
    }

    that.displayExpirationDatePicklist = function(expirationDates) {
        var selectedDate = $j("#ExpirationPickList option:selected").text();
        $j('#ExpirationPickList').html('');
        var optLabel = $j('<option value="Select">Select Date</option>');
        $j('#ExpirationPickList').append(optLabel);
        expirationDates.forEach(function(item) {
            var selected = selectedDate == item.Date_String__c ? 'selected ' : '' ;
            var optItem = $j('<option ' + selected + 'value="' + item.Date_String__c + '">' + item.Date_String__c + '</option>');
            $j('#ExpirationPickList').append(optItem);

        } );


    }

    return that;
}

function WelkParse() {
    var that = new BatchParse;


    that.parseData = function() {
        $j("#popupLoading").show();

        console.log('WelkParse', 'parseData')
        that.addHeaders();
        that.addRecords();
        that.getValidationData(); //Will call a Remoting function that calls validateWelkRecords() and displayRecords();
        console.log("Synchronous parse results:", inputData);
    }

    that.quickParse = function() {

        $j("#closingDate").show();
        $j("#expirationDate").hide();
        $j("#submit-parse").show();

        $j("#popupLoading").hide();
    }    

    that.addHeaders = function() {
        var i = 1;
        var columns = [];
        columns.push( {name: "Problems", label: "Problems", inputColumn: 0, visible: true} );
        columns.push( {name: "Verified", label: "Verified", inputColumn: 0, visible: true} );

        columns.push( {name: "MembershipNumber", label: "Membership Number", inputColumn: i++, visible: true} );
        columns.push( {name: "ContractNumber", label: "Contract Number", inputColumn: i++, visible: true} );
        columns.push( {name: "ContractDate", label: "Contract Date", inputColumn: i++, visible: true} );
        columns.push( {name: "BatchNumber", label: "Batch Number", inputColumn: i++, visible: true} );
        columns.push( {name: "Portfolio", label: "Portfolio", inputColumn: i++, visible: true} );
        columns.push( {name: "Name1", label: "Name 1", inputColumn: i++, visible: true} );
        columns.push( {name: "Name2", label: "Name 2", inputColumn: i++, visible: true} );
        columns.push( {name: "Address", label: "Address", inputColumn: i++, visible: true} );
        columns.push( {name: "City", label: "City", inputColumn: i++, visible: true} );
        columns.push( {name: "State", label: "State", inputColumn: i++, visible: true} );
        columns.push( {name: "Zip", label: "Zip", inputColumn: i++, visible: true} );
        columns.push( {name: "Country", label: "Country", inputColumn: i++, visible: true} );
        columns.push( {name: "Vesting", label: "Vesting .", inputColumn: i++, visible: true} );
        columns.push( {name: "Upgrd_Pts", label: "Upgrd Pts", inputColumn: i++, visible: true} );
        columns.push( {name: "Convrt_Pts", label: "Convrt Pts", inputColumn: i++, visible: true} );
        columns.push( {name: "New_Pts", label: "New Pts", inputColumn: i++, visible: true} );
        columns.push( {name: "SumPts", label: "Sum Pts", inputColumn: i++, visible: true} );
        columns.push( {name: "Occ_Year", label: "Occ Year", inputColumn: i++, visible: true} );
        columns.push( {name: "IC", label: "IC #", inputColumn: i++, visible: true} );
        columns.push( {name: "RC", label: "RC .", inputColumn: i++, visible: true} );
        columns.push( {name: "MCBP", label: "MC/BP", inputColumn: i++, visible: true} );
        columns.push( {name: "PurchasePrice", label: "Purchase Price", inputColumn: i++, visible: true} );
        columns.push( {name: "Conv_Fee", label: "Conv Fee", inputColumn: i++, visible: true} );
        columns.push( {name: "Conv_Value", label: "Conv Value", inputColumn: i++, visible: true} );
        columns.push( {name: "AmtFinance", label: "Amt Finance", inputColumn: i++, visible: true} );
        columns.push( {name: "Down_Pymt", label: "Down Pymt", inputColumn: i++, visible: true} );
        columns.push( {name: "Close_Equity", label: "Close Equity", inputColumn: i++, visible: true} );
        columns.push( {name: "Down_Equity", label: "Down Equity", inputColumn: i++, visible: true} );
        columns.push( {name: "Total_Equity", label: "Total Equity", inputColumn: i++, visible: true} );
        columns.push( {name: "ClsCost_Rcvd", label: "ClsCost Rcvd", inputColumn: i++, visible: true} );
        columns.push( {name: "TotCash_Rcvd", label: "TotCash Rcvd", inputColumn: i++, visible: true} );
        columns.push( {name: "CTT_Close_Cost", label: "CTT Close Cost", inputColumn: i++, visible: true} );
        columns.push( {name: "CloseCost", label: "Close Cost", inputColumn: i++, visible: true} );
        columns.push( {name: "Escrow_Proceeds", label: "Escrow Proceeds", inputColumn: i++, visible: true} );
        columns.push( {name: "Hypoth_Amount", label: "Hypoth Amount", inputColumn: i++, visible: true} );
        columns.push( {name: "Deeded_Date", label: "Deeded Date", inputColumn: i++, visible: true} );
        columns.push( {name: "FirstPaymentDueDate", label: "First Payment Due Date", inputColumn: i++, visible: true} );

        columns.push( {name:  "ClosingDate", label: "Closing Date", inputColumn: 0, visible: true} );
        columns.push( {name: "Trust", label: "Trust", inputColumn: 0, visible: true} );
        columns.push( {name: "Lender", label: "Lender", inputColumn: 0, visible: true} );
        columns.push( {name: "Season", label: "Season", inputColumn: 0, visible: true} );
        columns.push( {name: "ResortCode", label: "ResortCode", inputColumn: 0, visible: true} );
        columns.push( {name: "TotalDebits", label: "TotalDebits", inputColumn: 0, visible: true} );
        columns.push( {name: "TotalCredits", label: "TotalCredits", inputColumn: 0, visible: true} );
        columns.push( {name: "ICN", label: "ICN", inputColumn: 0, visible: true} );

        columns.push( {name:  "Name1Salutation", label: "Name 1 Salutation", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1First", label: "Name 1 First", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Middle", label: "Name 1 Middle", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Last", label: "Name 1 Last", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Suffix", label: "Name 1 Suffix", inputColumn: 0, visible: true} );

        columns.push( {name:  "Name2Salutation", label: "Name 2 Salutation", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2First", label: "Name 2 First", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Middle", label: "Name 2 Middle", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Last", label: "Name 2 Last", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Suffix", label: "Name 2 Suffix", inputColumn: 0, visible: true} );

        columns.push( {name: "LenderId", label: "Lender Id", inputColumn: 0, visible: true} );
        columns.push( {name: "ResortId", label: "Resort Id", inputColumn: 0, visible: true} );
        columns.push( {name: "ExpirationDateId", label: "Expiration Date Id", inputColumn: 0, visible: true} );
        columns.push( {name: "MembershipTypeId", label: "Membership Type Id", inputColumn: 0, visible: true} );
        columns.push( {name: "MemberTypeId", label: "Member Type Id", inputColumn: 0, visible: true} );
        columns.push( {name: "TransactionTypeId", label: "Transaction Type Id", inputColumn: 0, visible: true} );

        inputData.headers = columns;
    }

    that.addRecords = function() {

        var closingDate = $j("#closingDatePicker").val();
        inputData.records = [];
        inputData.data.forEach(function(item) {
            if (item.length < 37) {return;} // intended to remove a trailing new line row
            var rowData = {};
            rowData.isError = false;
            rowData.MembershipNumber = {value: item[0].trim(), valid: true};
            rowData.ContractNumber = {value: item[1].trim(), valid: true};
            rowData.ContractDate = {value: item[2], valid: true};
            rowData.BatchNumber = {value: item[3].trim(), valid: true};
            rowData.Portfolio = {value: item[4].trim(), valid: true};
            rowData.Name1 = {value: item[5].trim(), valid: true};
            rowData.Name1Salutation = {value: '', valid: true};
            rowData.Name1First = {value: '', valid: true};
            rowData.Name1Middle = {value: '', valid: true};
            rowData.Name1Last = {value: '', valid: true};
            rowData.Name1Suffix = {value: '', valid: true};
            rowData.Name2 = {value: item[6].trim(), valid: true};
            rowData.Name2Salutation = {value: '', valid: true};
            rowData.Name2First = {value: '', valid: true};
            rowData.Name2Middle = {value: '', valid: true};
            rowData.Name2Last = {value: '', valid: true};
            rowData.Name2Suffix = {value: '', valid: true};
            rowData.Address = {value: item[7].trim(), valid: true};
            rowData.City = {value: item[8].trim(), valid: true};
            rowData.State = {value: item[9].trim(), valid: true};
            rowData.Zip ={value: item[10].trim(), valid: true};
            rowData.Country = {value: item[11].trim(), valid: true};
            rowData.Vesting = {value: item[12].trim(), valid: true};
            rowData.Upgrd_Pts = {value: item[13].trim(), valid: true};
            rowData.Convrt_Pts = {value: item[14].trim(), valid: true};
            rowData.New_Pts = {value: item[15].trim(), valid: true};
            rowData.SumPts = {value: item[16].trim(), valid: true};
            rowData.Occ_Year = {value: item[17].trim(), valid: true};
            rowData.IC = {value: item[18].trim(), valid: true};
            rowData.RC = {value: item[19].trim(), valid: true};
            rowData.MCBP = {value: item[20].trim(), valid: true};
            rowData.PurchasePrice = {value: item[21].trim(), valid: true};
            rowData.Conv_Fee = {value: item[22].trim(), valid: true};
            rowData.Conv_Value = {value: item[23].trim(), valid: true};
            rowData.AmtFinance = {value: item[24].trim(), valid: true};
            rowData.Down_Pymt = {value: item[25].trim(), valid: true};
            rowData.Close_Equity = {value: item[26].trim(), valid: true};
            rowData.Down_Equity = {value: item[27].trim(), valid: true};
            rowData.Total_Equity = {value: item[28].trim(), valid: true};
            rowData.ClsCost_Rcvd = {value: item[29].trim(), valid: true};
            rowData.TotCash_Rcvd = {value: item[30].trim(), valid: true};
            rowData.CTT_Close_Cost = {value: item[31].trim(), valid: true};
            rowData.CloseCost = {value: item[32].trim(), valid: true};
            rowData.Escrow_Proceeds = {value: item[33].trim(), valid: true};
            rowData.Hypoth_Amount = {value: item[34].trim(), valid: true};
            rowData.Deeded_Date = {value: item[35].trim(), valid: true};
            rowData.FirstPaymentDueDate = {value: item[36].trim(), valid: true};

            rowData.Problems = {value: '', valid: true};
            rowData.Verified = {value: 'false', valid: true};
            rowData.ClosingDate = {value: closingDate, valid: true};
            rowData.Trust = {value: '', valid: true};
            rowData.Lender = {value: '', valid: true};
            rowData.Season = {value: '', valid: true};
            rowData.ResortCode = {value: '', valid: true};
            rowData.TotalDebits = {value: '', valid: true};
            rowData.TotalCredits = {value: '', valid: true};
            rowData.ICN = {value: '', valid: true};

            rowData.LenderId = {value: '', valid: true};
            rowData.ResortId = {value: '', valid: true};
            rowData.ExpirationDateId = {value: '', valid: true};
            rowData.MembershipTypeId = {value: '', valid: true};
            rowData.MemberTypeId = {value: '', valid: true};
            rowData.TransactionTypeId = {value: '', valid: true};

            if (rowData.Name1.value != '') {
                var n1 = parseFullName(rowData.Name1.value);

                rowData.Name1Salutation = {value: n1.title, valid: true};
                rowData.Name1First = {value: n1.first, valid: true};
                rowData.Name1Middle = {value: n1.middle, valid: true};
                rowData.Name1Last = {value: n1.last, valid: true};
                rowData.Name1Suffix = {value: n1.suffix, valid: true};
            }

            if (rowData.Name2.value != '') {
                var n2 = parseFullName(rowData.Name2.value);
                rowData.Name2Salutation = {value: n2.title, valid: true};
                rowData.Name2First = {value: n2.first, valid: true};
                rowData.Name2Middle = {value: n2.middle, valid: true};
                rowData.Name2Last = {value: n2.last, valid: true};
                rowData.Name2Suffix = {value: n2.suffix, valid: true};
            }


            // Fix Lender
            var portfolio = rowData.Portfolio.value;
            if (portfolio.includes('Cap One')){
                rowData.Lender.value = "Capital One";
            }
            if (portfolio.includes('ouse')){
                rowData.Lender.value = "Welk Resort Group, Inc.";
            }
            if (portfolio.includes('Source')){
                rowData.Lender.value = "Capital Source";
            }
            if (portfolio.includes('Cole')){
                rowData.Lender.value = "Colebrook Financial Company, LLC";
            }
            if (portfolio.includes('iego')){
                rowData.Lender.value = "San Diego National Bank";
            }
            if (portfolio.includes('Arizona')){
                rowData.Lender.value = "National Bank of Arizona";
            }
            if (portfolio.includes('Textron')){
                rowData.Lender.value = "Textron Financial Corporation, a Delaware corp.";
            }
            if (rowData.Lender.value == ''){
                rowData.Lender.value = rowData.Portfolio.value;
            }

            // Calculate Season and ResortCode
            //if ResortCode (IC) contains "/"  (eg PTC2/A)
            //	Season = ResortCode - last character
            //	ResortCode = ResortCode - minus last two characters
            if (rowData.IC.value.includes('/')){
                var resort = rowData.IC.value.split('/');
                rowData.ResortCode.value = resort[0];
                rowData.Season.value = resort[1];
            } else {
                rowData.ResortCode.value = rowData.IC.value;
            }



            // validate row and add
            if (rowData.MembershipNumber.value != '' && rowData.MembershipNumber.value != '#' && rowData.MembershipNumber.value != 'Own'
                && rowData.ContractNumber.value != '' && rowData.ContractNumber.value != '.' && rowData.ContractNumber.value != 'Cont'
                && rowData.SumPts.value != '') {
                inputData.records.push(rowData);
            }
        });

    }
    that.getValidationData = function() {
        var contractNumberList = [];
        var lenderList = [];
        var resortCodeList = [];
        var records = inputData.records;

        records.forEach(function (record) {
            contractNumberList.push(record.ContractNumber.value);
            lenderList.push(record.Lender.value);
            resortCodeList.push(record.ResortCode.value);
        });
        // JS Remoting Call to get records out of SF
        /// ASYNCHRONOUS call ///
        BatchPointsController.getWelkValidationData(
            contractNumberList.filter(onlyUnique), lenderList.filter(onlyUnique), resortCodeList.filter(onlyUnique),
            function(result) {
                console.log('Async getWelkValidationData result', result);
                result.Contract__c.forEach(function(item) { that.sfContracts[item.Contract_Number__c] = item.Id; });
                result.Lender__c.forEach(function(item) { that.sfLenders[item.Name] = item.Id; inputData.meta.developerId = item.Developer__c; });
                result.Resort__c.forEach(function(item) { that.sfResort[item.Code__c] = item.Id; });
                result.Transaction_Type__c.forEach(function(item) { that.sfTransactionType["Issued"] = item.Id; });
                result.Member_Type__c.forEach(function(item) { that.sfMemberType["Flex"] = item.Id; });

                records.forEach(function (record) {
                    if (record.Lender.value != '') record.LenderId.value = that.sfLenders[record.Lender.value];
                    if (record.ResortCode.value != '') record.ResortId.value = that.sfResort[record.ResortCode.value];
                    record.MemberTypeId.value = that.sfMemberType["Flex"];
                    record.TransactionTypeId.value = that.sfTransactionType["Issued"];

                });

                that.validateRecords();
                that.displayRecords();
                $j("#closingDate, #submit-load, #dataTable").show();
            }
        );
        ////////////////////////
    }

    that.validateRecords = function() {
        var records = inputData.records;

        records.forEach(function (record) {
            // Contract Number contains P-
            if (!record["ContractNumber"].value.includes('P-')){
                record["ContractNumber"].valid = false;
                var problemMessage = 'ContractNumber is invalid';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }


            // Validate Season is one of A, E, O
            if ( !(['A', 'E', 'O'].indexOf(record["Season"].value) >= 0) ){
                record["Season"].valid = false;
                record["IC"].valid = false;
                var problemMessage = 'Season needs to have A, E or O';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Data lengths
            if (record["BatchNumber"].value.length > 10){
                record["BatchNumber"].valid = false;
                var problemMessage = 'BatchNumber longer than 10 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["ResortCode"].value.length > 50){
                record["ResortCode"].valid = false;
                var problemMessage = 'ResortCode longer than 50 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Season"].value.length > 50){
                record["Season"].valid = false;
                var problemMessage = 'Season longer than 50 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["ContractNumber"].value.length > 20){
                record["ContractNumber"].valid = false;
                var problemMessage = 'ContractNumber longer than 20 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["ContractNumber"].value.length > 20){
                record["ContractNumber"].valid = false;
                var problemMessage = 'ContractNumber longer than 20 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["MembershipNumber"].value.length > 18){
                record["MembershipNumber"].valid = false;
                var problemMessage = 'MembershipNumber longer than 18 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Name1"].value.length > 50){
                record["Name1"].valid = false;
                var problemMessage = 'Name1 longer than 50 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Name2"].value.length > 50){
                record["Name2"].valid = false;
                var problemMessage = 'Name2 longer than 50 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Trust"].value.length > 250){
                record["Trust"].valid = false;
                var problemMessage = 'Trust longer than 250 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Currency fields valid
            if (!isNumeric(record["PurchasePrice"].value)){
                record["PurchasePrice"].valid = false;
                var problemMessage = 'PurchasePrice price wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["AmtFinance"].value)){
                record["AmtFinance"].valid = false;
                var problemMessage = 'AmtFinance wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["CloseCost"].value)){
                record["CloseCost"].valid = false;
                var problemMessage = 'CloseCost wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Validate not blank
            if (record["MembershipNumber"].value == ""){
                record["MembershipNumber"].valid = false;
                var problemMessage = 'MembershipNumber is blank';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Name1"].value == ""){
                record["Name1"].valid = false;
                var problemMessage = 'Name1 is blank';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Address"].value == ""){
                record["Address"].valid = false;
                var problemMessage = 'Address is blank';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["City"].value == ""){
                record["City"].valid = false;
                var problemMessage = 'City is blank';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["BatchNumber"].value == ""){
                record["BatchNumber"].valid = false;
                var problemMessage = 'BatchNumber is blank';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }


            // Points are numeric
            if (!isNumeric(record["Upgrd_Pts"].value)){
                record["Upgrd_Pts"].valid = false;
                var problemMessage = 'Upgrd_Pts is not a point value';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["Convrt_Pts"].value)){
                record["Convrt_Pts"].valid = false;
                var problemMessage = 'Convrt Pts is not a point value';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["New_Pts"].value)){
                record["New_Pts"].valid = false;
                var problemMessage = 'New Pts is not a point value';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["SumPts"].value)){
                record["SumPts"].valid = false;
                var problemMessage = 'SumPts is not a point value';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Contract Date is a date
            if (!Date.parse(record["ContractDate"].value)){
                record["ContractDate"].valid = false;
                var problemMessage = 'ContractDate is not a valid date';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Calculate Debits and Credits
            record["TotalDebits"].value =  (Number((record["PurchasePrice"].value).replace(/[$,]+/g,"")) + Number((record["CloseCost"].value).replace(/[$,]+/g,""))).toFixed(2);
            record["TotalCredits"].value =	(Number((record["PurchasePrice"].value).replace(/[$,]+/g,"")) + Number((record["CloseCost"].value).replace(/[$,]+/g,""))).toFixed(2);

            // Validate Contract Number is not duplicate
            if (that.sfContracts[record.ContractNumber.value] != null ) {
                record.ContractNumber.valid = false;
                var problemMessage = 'Contract Number already exists in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Lender validate exists in db
            if (that.sfLenders[record.Lender.value] == null ) {
                record.Lender.valid = false;
                var problemMessage = 'Lender does not exist in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            //  ResortCode validate exists in db SELECT COUNT(*) FROM Resorts WHERE Id =
            if (that.sfResort[record.ResortCode.value] == null ) {
                record.ResortCode.valid = false;
                var problemMessage = 'Resort does not exist in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }


            record["Verified"] = record["isError"] ? {value: 'false', valid: false} : {value: 'true', valid: true};
			inputData.meta.unverified = record["isError"] ? ++inputData.meta.unverified : inputData.meta.unverified;


        });
    }

    that.loadData = function() {
        if(inputData == null) {return;}

        $j("#popupLoading").show();
        var dataRows = [];
        var rowId = 0;
        var batchType = 'Welk';
        var batchId = 'WELK|' + Date.parse(new Date());
        console.log('Batch Number:', batchId);
        inputData.records.forEach(function(item) {
            //if(item.isError == false) {
            var rowCounter = ++rowId;
            var bc = new Points_Batch_Closing__c();
            bc.Name = batchId + '|' + ('00000' + rowCounter).substr(-5);
            bc.Import_Batch_Identifier__c = batchId;
            bc.Import_Batch_Row__c = rowCounter;
            bc.Owner__c = item.MembershipNumber.value;
            bc.Contract__c = item.ContractNumber.value;
            if (!isNaN(Date.parse(item.ContractDate.value))) bc.Contract_Date__c = item.ContractDate.value.ToDate();
            bc.Batch__c = item.BatchNumber.value.ToNumber();
            bc.Portfolio__c = item.Portfolio.value;

            bc.Name_1__c = item.Name1.value;
            bc.Name_1_Salutation__c = item.Name1Salutation.value;
            bc.Name_1_First__c = item.Name1First.value;
            bc.Name_1_Middle__c = item.Name1Middle.value;
            bc.Name_1_Last__c = item.Name1Last.value;
            bc.Name_1_Suffix__c = item.Name1Suffix.value;
            bc.Name_2__c = item.Name2.value;
            bc.Name_2_Salutation__c = item.Name2Salutation.value;
            bc.Name_2_First__c = item.Name2First.value;
            bc.Name_2_Middle__c = item.Name2Middle.value;
            bc.Name_2_Last__c = item.Name2Last.value;
            bc.Name_2_Suffix__c = item.Name2Suffix.value;

            bc.Owner_Address__c = item.Address.value;
            bc.Owner_City__c = item.City.value;
            bc.Owner_State__c = item.State.value;
            bc.Owner_Postal_Code__c = item.Zip.value;
            bc.Owner_Country__c = item.Country.value;
            bc.Vesting__c = item.Vesting.value;
            bc.Upgrade_Points__c = item.Upgrd_Pts.value.ToNumber();
            bc.Convert_Points__c = item.Convrt_Pts.value.ToNumber();
            bc.New_Points__c = item.New_Pts.value.ToNumber();
            bc.Sum_Points__c = item.SumPts.value.ToNumber();
            bc.Occupancy_Year__c = item.Occ_Year.value;
            bc.IC_Number__c = item.IC.value;
            bc.RC__c = item.RC.value;
            bc.MC_BP__c = item.MCBP.value;
            bc.Purchase_Price__c = item.PurchasePrice.value.ToNumber();
            bc.Conv_Fee__c = item.Conv_Fee.value.ToNumber();
            bc.Conv_Value__c = item.Conv_Value.value.ToNumber();
            bc.Amt_Finance__c = item.AmtFinance.value.ToNumber();
            bc.Down_Payment__c = item.Down_Pymt.value.ToNumber();
            bc.Close_Equity__c = item.Close_Equity.value.ToNumber();
            bc.Down_Equity__c = item.Down_Equity.value.ToNumber();
            bc.Total_Equity__c = item.Total_Equity.value.ToNumber();
            bc.Closing_Cost_Received__c = item.ClsCost_Rcvd.value.ToNumber();
            bc.Total_Cash_Received__c = item.TotCash_Rcvd.value.ToNumber();
            bc.CTT_Close_Cost__c = item.CTT_Close_Cost.value.ToNumber();
            bc.Closing_Costs__c = item.CloseCost.value.ToNumber();
            bc.Escrow_Proceeds__c = item.Escrow_Proceeds.value.ToNumber();
            bc.Hypoth_Amount__c = item.Hypoth_Amount.value.ToNumber();
            if (!isNaN(Date.parse(item.Deeded_Date.value))) bc.Deeded_Date__c = item.Deeded_Date.value.ToDate();
            if (!isNaN(Date.parse(item.FirstPaymentDueDate.value))) bc.First_Payment_Due_Date__c = item.FirstPaymentDueDate.value.ToDate();
            bc.Trust__c = item.Trust.value;

            bc.Problems__c = item.Problems.value;
            bc.Verified__c = item.Verified.value == 'true';

            bc.Lender__c = item.Lender.value;
            bc.Season__c = item.Season.value;
            bc.Resort_Code__c = item.ResortCode.value;
            bc.Total_Debits__c = item.TotalDebits.value.ToNumber();
            bc.Resort_Name__c = item.ICN.value;
            bc.Total_Credits__c = item.TotalCredits.value.ToNumber();

            bc.Closing_Date__c = item.ClosingDate.value.ToDate();

            bc.Lender_Id__c = item.LenderId.value;
            bc.Resort__c = item.ResortId.value;
            bc.Expiration_Date_Id__c = item.ExpirationDateId.value;
            bc.Membership_Type_Id__c = item.MembershipTypeId.value;
            bc.Member_Type_Id__c = item.MemberTypeId.value;
            bc.Transaction_Type_Id__c = item.TransactionTypeId.value;


            dataRows.push(bc);
            //}
        });

        BatchPointsController.loadData(
            dataRows, batchType, batchId, inputData.meta.developerId,
            function(result) {
                console.log('Async loadData result: ', result);
                //TODO: provide UI feedback of success or failure
                // Update UI with restults information
                var bi = $j('<span />');
                for(var prop in result.errors) {
                    console.log(prop + '=' + result.errors[prop]);
                }
                bi.append('<strong>Records created in Salesforce:</strong>');
                bi.append('<br />');
                bi.append('Membership Number records: ' + result.records['Membership_Number__c']);
                bi.append('<br />');
                bi.append('Contract records: ' + result.records['Contract__c']);
                bi.append('<br />');
                bi.append('Transaction records: ' + result.records['Transaction__c']);
                bi.append('<br />');
                bi.append('Owner records: ' + result.records['Owner__c']);
                bi.append('<br />');
                bi.append('Owner List records: ' + result.records['Owner_List__c']);
                bi.append('<br />');
                bi.append('<br />');
                $j('#insertResult').html(bi);

                var link = $j('<span />');
                link.append('<strong>Link to this batch record:</strong>');
                link.append('<br />');
                var anchor = $j('<a href="/' + result.records['Points_Batch_Header__c'] + '">Batch Record</a>');
                link.append(anchor);
                $j('#batchRecordLink').html(link);
                $j('#submit-load').hide();

                $j("#popupLoading").hide();
            }
        );
    }



    return that;
}

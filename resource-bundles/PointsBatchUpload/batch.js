$j = jQuery.noConflict();
var inputData;
var bp = {}; 


$j(document).ready(function($) {
    $j("#popupLoading").show();

    bp = new BatchParse; 
    $j('#closingDatePicker').val(new Date().toDateInputValue());    
/*    $j('#inputText').on("paste", function(){
        setTimeout(function() {
            $j("#popupLoading").show();
            getInputData();
            getParser();
            setButtonEvents();
            bp.quickParse(); 

        }, 500);
    });*/
    $j('#inputText').on("input selectionchange propertychange", function(){
			$j("#popupLoading").show();
            getInputData();
			if (inputData.meta.developer != '') {
				getParser();
				setButtonEvents();
				bp.quickParse();
			}
			$j("#popupLoading").hide();
    });

    $j("#closingDate, #expirationDate, #submit-load, #dataTable, #submit-parse").hide();
    $j("#popupLoading").hide();
});

function getInputData() {
    var txt = $j('#inputText').val();
    inputData = Papa.parse(txt, {delimiter: "",newline: ""});
    inputData.meta.unverified = 0;
	inputData.meta.developer = '';
    if (inputData.data[0].length == 37) inputData.meta.developer = 'Welk';
	if (inputData.data[0].length == 31) inputData.meta.developer = 'Shell';
	//inputData.meta.developer = inputData.data[0].length > 35 ? 'Welk' : 'Shell';
	console.log("Input data column count: ", inputData.data[0].length);
	console.log(inputData.meta.developer);
}

function getParser() {

    if (inputData.meta.developer == 'Welk') { bp = WelkParse(); }
    if (inputData.meta.developer == 'Shell') { bp = ShellParse(); }
}

function setButtonEvents() {
    $j('#submit-parse').click(bp.parseData);
    $j('#ExpirationPickList').change(bp.parseData);
    $j('#closingDatePicker').change(bp.parseData);
    $j('#submit-load').click(bp.loadData);
}



function BatchParse() {
    var that = {};
    that.sfContracts = {};
    that.sfLenders = {};
    that.sfMembershipType = {};
    that.sfResort = {};
    that.sfExpirationDate = {};
    that.sfMemberType = {};
    that.sfTransactionType = {};


    that.displayRecords = function() {
        $j("#dataTable").show();
        var table = $j('<table />');
        var headers = inputData.headers;
        var records = inputData.records;

        if ( headers == null ) return;
        var headerRow = $j('<tr />');
        headers.forEach(function (header) {
            if(header.visible == true) {
                headerRow.append('<th>'+ header.label+'</th>');
            }
        });
        table.append(headerRow);
        if (records != null) {
            records.forEach(function (record, index) {
                if (!record.isError) {
                    row = $j('<tr />');
                } else {
                    row = $j('<tr class="rowError" />');
                }
                headers.forEach(function (header, index) {
                    if(header.visible == true) {
                        if (header.inputColumn != 0) {
                            if (record[header.name].valid == true) {
                                row.append('<td>' + record[header.name].value + '</td>');
                            } else {
                                row.append('<td class="cellError">' + record[header.name].value + '</td>');
                            }
                        } else {
                            if (record[header.name].valid == true) {
                                row.append('<td class="cellCalculated">' + record[header.name].value + '</td>');
                            } else {
                                row.append('<td class="cellError">' + record[header.name].value + '</td>');
                            }
                        }
                    }


                });
                table.append(row);
            });
        }
        $j('#parsedDataTable').html(table);

        // Update UI with restults information
        var bi = $j('<span />');
        bi.append('<strong>Parse Results:</strong>');
        bi.append($j('<br/>'));
        bi.append('Source format: ' + inputData.meta.developer);
        bi.append($j('<br/>'));
        bi.append(inputData.records.length + ' Records');
        bi.append($j('<br/>'));
        if (inputData.meta.unverified != 1) {
            bi.append(inputData.meta.unverified + ' Unverified Records');
        } else {
            bi.append(inputData.meta.unverified + ' Unverified Record');
        }
        $j('#parseResult').html(bi);
        $j("#popupLoading").hide();
    }

    return that;

}




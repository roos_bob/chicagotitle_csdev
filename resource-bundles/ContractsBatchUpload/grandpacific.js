function GrandPacificParse() {
    var that = new BatchParse; 

    that.parseData = function() {
        $j("#popupLoading").show();

        console.log('GrandPacificParse', 'parseData')
        that.addHeaders();
        that.addRecords();
        that.getValidationData(); //Will call a Remoting function that will in turn call validateRecords() and displayRecords();
        console.log("Synchronous parse results:", inputData);
    }
	/*
		function to run immediately after text is pasted into the input textbox
		If no prompts are needed, this can call the full parse function
	*/
    that.quickParse = function() {

        $j("#closingDate").show();
        $j("#expirationDate").hide();
        $j("#submit-parse").show();

        $j("#popupLoading").hide();
    }    
	/*
		Define column headers
	*/
    that.addHeaders = function() {
        var i = 1;
        var columns = [];
        columns.push( {name: "Problems", label: "Problems", inputColumn: 0, visible: true} );
        columns.push( {name: "Verified", label: "Verified", inputColumn: 0, visible: true} );

		columns.push( {name: "ResortName", label: "ResortName", inputColumn: i++, visible: true} );
		columns.push( {name: "Contract", label: "Contract", inputColumn: i++, visible: true} );
		columns.push( {name: "ContractId", label: "ContractId", inputColumn: i++, visible: true} );
		columns.push( {name: "SalesPerson", label: "SalesPerson", inputColumn: i++, visible: true} );
		columns.push( {name: "TO", label: "TO", inputColumn: i++, visible: true} );
		columns.push( {name: "QAM", label: "QAM", inputColumn: i++, visible: true} );
		columns.push( {name: "CustomerName1", label: "CustomerName1", inputColumn: i++, visible: true} );
		columns.push( {name: "CustomerName2", label: "CustomerName2", inputColumn: i++, visible: true} );
		columns.push( {name: "Source", label: "Source", inputColumn: i++, visible: true} );
		columns.push( {name: "Lender", label: "Lender", inputColumn: i++, visible: true} );
		columns.push( {name: "ContractPrice", label: "ContractPrice", inputColumn: i++, visible: true} );
		columns.push( {name: "SalesDate", label: "SalesDate", inputColumn: i++, visible: true} );
		columns.push( {name: "Financed", label: "Financed", inputColumn: i++, visible: true} );
		columns.push( {name: "PreviousLoanTransfer", label: "PreviousLoanTransfer", inputColumn: i++, visible: true} );
		columns.push( {name: "FundedAmount", label: "FundedAmount", inputColumn: i++, visible: true} );
		columns.push( {name: "TransferEquity", label: "TransferEquity", inputColumn: i++, visible: true} );
		columns.push( {name: "DownPayment", label: "DownPayment", inputColumn: i++, visible: true} );
		columns.push( {name: "PercentOfDP", label: "PercentOfDP", inputColumn: i++, visible: true} );
		columns.push( {name: "ClosingCost", label: "ClosingCost", inputColumn: i++, visible: true} );
		columns.push( {name: "FICO", label: "FICO", inputColumn: i++, visible: true} );
		columns.push( {name: "Interval", label: "Interval", inputColumn: i++, visible: true} );
		columns.push( {name: "Term", label: "Term", inputColumn: i++, visible: true} );
		columns.push( {name: "APR", label: "APR", inputColumn: i++, visible: true} );
		columns.push( {name: "PayAmount", label: "PayAmount", inputColumn: i++, visible: true} );
		columns.push( {name: "FirstPayDate", label: "FirstPayDate", inputColumn: i++, visible: true} );
		columns.push( {name: "NetCommissionSalesPrice", label: "NetCommissionSalesPrice", inputColumn: i++, visible: true} );
		columns.push( {name: "SalesCenter", label: "SalesCenter", inputColumn: i++, visible: true} );
		columns.push( {name: "FdiPointsProvided", label: "FdiPointsProvided", inputColumn: i++, visible: true} );
		columns.push( {name: "tu_unit_number1", label: "tu_unit_number1", inputColumn: i++, visible: true} );
		columns.push( {name: "wk_number1", label: "wk_number1", inputColumn: i++, visible: true} );
		columns.push( {name: "Usage", label: "Usage", inputColumn: i++, visible: true} );
		columns.push( {name: "inventory_usage_type1", label: "inventory_usage_type1", inputColumn: i++, visible: true} );
		columns.push( {name: "Ownership", label: "Ownership", inputColumn: i++, visible: true} );
		columns.push( {name: "CommissionDiscountAmount", label: "CommissionDiscountAmount", inputColumn: i++, visible: true} );
		columns.push( {name: "FdiPointsAuthorized", label: "FdiPointsAuthorized", inputColumn: i++, visible: true} );

        columns.push( {name:  "Name1Salutation", label: "Name 1 Salutation", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1First", label: "Name 1 First", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Middle", label: "Name 1 Middle", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Last", label: "Name 1 Last", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name1Suffix", label: "Name 1 Suffix", inputColumn: 0, visible: true} );

        columns.push( {name:  "Name2Salutation", label: "Name 2 Salutation", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2First", label: "Name 2 First", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Middle", label: "Name 2 Middle", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Last", label: "Name 2 Last", inputColumn: 0, visible: true} );
        columns.push( {name:  "Name2Suffix", label: "Name 2 Suffix", inputColumn: 0, visible: true} );

		columns.push( {name: "ResortId", label: "Resort Id", inputColumn: 0, visible: true} );
		columns.push( {name: "UnitId", label: "Unit Id", inputColumn: 0, visible: true} );

        inputData.headers = columns;
    }
	/*
		Add raw data to fields in each row
	*/
    that.addRecords = function() {

        var closingDate = $j("#closingDatePicker").val();
        inputData.records = [];
        inputData.data.forEach(function(item) {
            if (item.length < 35) {return;} // intended to remove a trailing new line row
            var rowData = {};
            rowData.isError = false;
			rowData.ResortName = {value: item[0].trim(), valid: true};
			rowData.Contract = {value: item[1].trim(), valid: true};
			rowData.ContractId = {value: item[2].trim(), valid: true};
			rowData.SalesPerson = {value: item[3].trim(), valid: true};
			rowData.TO = {value: item[4].trim(), valid: true};
			rowData.QAM = {value: item[5].trim(), valid: true};
			rowData.CustomerName1 = {value: item[6].trim(), valid: true};
			rowData.CustomerName2 = {value: item[7].trim(), valid: true};
			rowData.Source = {value: item[8].trim(), valid: true};
			rowData.Lender = {value: item[9].trim(), valid: true};
			rowData.ContractPrice = {value: item[10].trim(), valid: true};
			rowData.SalesDate = {value: item[11].trim(), valid: true};
			rowData.Financed = {value: item[12].trim(), valid: true};
			rowData.PreviousLoanTransfer = {value: item[13].trim(), valid: true};
			rowData.FundedAmount = {value: item[14].trim(), valid: true};
			rowData.TransferEquity = {value: item[15].trim(), valid: true};
			rowData.DownPayment = {value: item[16].trim(), valid: true};
			rowData.PercentOfDP = {value: item[17].trim(), valid: true};
			rowData.ClosingCost = {value: item[18].trim(), valid: true};
			rowData.FICO = {value: item[19].trim(), valid: true};
			rowData.Interval = {value: item[20].trim(), valid: true};
			rowData.Term = {value: item[21].trim(), valid: true};
			rowData.APR = {value: item[22].trim(), valid: true};
			rowData.PayAmount = {value: item[23].trim(), valid: true};
			rowData.FirstPayDate = {value: item[24].trim(), valid: true};
			rowData.NetCommissionSalesPrice = {value: item[25].trim(), valid: true};
			rowData.SalesCenter = {value: item[26].trim(), valid: true};
			rowData.FdiPointsProvided = {value: item[27].trim(), valid: true};
			rowData.tu_unit_number1 = {value: item[28].trim(), valid: true};
			rowData.wk_number1 = {value: item[29].trim(), valid: true};
			rowData.Usage = {value: item[30].trim(), valid: true};
			rowData.inventory_usage_type1 = {value: item[31].trim(), valid: true};
			rowData.Ownership = {value: item[32].trim(), valid: true};
			rowData.CommissionDiscountAmount = {value: item[33].trim(), valid: true};
			rowData.FdiPointsAuthorized = {value: item[34].trim(), valid: true};

            rowData.Name1Salutation = {value: '', valid: true};
            rowData.Name1First = {value: '', valid: true};
            rowData.Name1Middle = {value: '', valid: true};
            rowData.Name1Last = {value: '', valid: true};
            rowData.Name1Suffix = {value: '', valid: true};
            rowData.Name2Salutation = {value: '', valid: true};
            rowData.Name2First = {value: '', valid: true};
            rowData.Name2Middle = {value: '', valid: true};
            rowData.Name2Last = {value: '', valid: true};
            rowData.Name2Suffix = {value: '', valid: true};

			rowData.ResortId = {value: '', valid: true};
			rowData.UnitId = {value: '', valid: true};

            rowData.Problems = {value: '', valid: true};
            rowData.Verified = {value: 'false', valid: true};

            if (rowData.CustomerName1.value != '') {
                var n1 = parseFullName(rowData.CustomerName1.value);

                rowData.Name1Salutation = {value: n1.title, valid: true};
                rowData.Name1First = {value: n1.first, valid: true};
                rowData.Name1Middle = {value: n1.middle, valid: true};
                rowData.Name1Last = {value: n1.last, valid: true};
                rowData.Name1Suffix = {value: n1.suffix, valid: true};
            }

            if (rowData.CustomerName2.value != '') {
                var n2 = parseFullName(rowData.CustomerName2.value);
                rowData.Name2Salutation = {value: n2.title, valid: true};
                rowData.Name2First = {value: n2.first, valid: true};
                rowData.Name2Middle = {value: n2.middle, valid: true};
                rowData.Name2Last = {value: n2.last, valid: true};
                rowData.Name2Suffix = {value: n2.suffix, valid: true};
            }

            // validate row contains data and add  (If a row contains labels instead of data, don't add it)
            if (rowData.ResortName.value != 'ResortName' && rowData.ResortName.value != ''&& rowData.Contract.value != '' ) {
                inputData.records.push(rowData);
            }
        });

    }
	/*
		Call Salesforce to get data used in validation of incoming data
	*/
    that.getValidationData = function() {
        var contractNumberList = [];
        var resortNameList = [];
        var unitNumberList = {};
        var records = inputData.records;
		console.log(inputData.records);
        records.forEach(function (record) {
            contractNumberList.push(record.Contract.value);
            resortNameList.push(record.ResortName.value);
			if (record.ResortName.value != '' && record.tu_unit_number1.value != '' ) {
				if (unitNumberList[record.ResortName.value] == null) {unitNumberList[record.ResortName.value] = [];}
				unitNumberList[record.ResortName.value].push(record.tu_unit_number1.value);
			}
        });
        // JS Remoting Call to get records out of SF
        /// ASYNCHRONOUS call ///
		BatchContractsController.getGrandPacificValidationData(
            contractNumberList.filter(onlyUnique), 
			resortNameList.filter(onlyUnique),
			unitNumberList,
            function(result) {
                console.log('Async getGrandPacificValidationData result', result);
                result.Resort__c.forEach(function(item) { that.sfResort[item.Name] = item.Id; inputData.meta.developerId = item.Developer__c; });
                result.Deeded_Contract__c.forEach(function(item) { that.sfContracts[item.Name] = item.Id; });
				
				for (var property in result) {  
					if (result.hasOwnProperty(property)) {
						if (property != 'Resort__c' && property != 'Deeded_Contract__c') { // property holds the name of a resort
							result[property].forEach(function(item) { 
								that.sfUnits[property + '-' + item.Unit_Code__c] = item.Id; 
							});
						}
					}
				}
				records.forEach(function (record) {
                    if (record.ResortName.value != '') record.ResortId.value = that.sfResort[record.ResortName.value];
					if (record.tu_unit_number1.value != '' && that.sfUnits[record.ResortName.value + '-' + record.tu_unit_number1.value] != null) {
						record.UnitId.value = that.sfUnits[record.ResortName.value + '-' + record.tu_unit_number1.value];
					}

                });
                that.validateRecords();
                that.displayRecords();
                $j("#closingDate, #submit-load, #dataTable").show();
            }
			
			
        );
		//that.displayRecords();  // copied here while we skip validation
		//inputData.meta.developerId = 'a11W00000015yub';
		$j("#submit-load").show();
		
        ////////////////////////
    }
	/*
		Function to perform the full validation of incoming raw data

	*/
    that.validateRecords = function() {
        var records = inputData.records;

        records.forEach(function (record) {
            // Interval is Annual, Odd, or Even
            if (
				record["inventory_usage_type1"].value != 'Annual' 
				&& record["inventory_usage_type1"].value != 'Odd' 
				&& record["inventory_usage_type1"].value != 'Even' ) 
			{
                record["inventory_usage_type1"].valid = false;
                var problemMessage = 'A valid usage type has not been provided.  Must be Annual, Odd, or Even.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Interval is Annual, Odd, or Even
            if (record["tu_unit_number1"].value == '' ) {
                record["tu_unit_number1"].valid = false;
                var problemMessage = 'No Unit Number provided.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Week Number is not blank
            if (record["wk_number1"].value == '' ) {
                record["wk_number1"].valid = false;
                var problemMessage = 'Week Number not provided.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            // Week Number is between 1 and 52
            if (!(record["wk_number1"].value > 0 && record["wk_number1"].value <53) ) {
                record["wk_number1"].valid = false;
                var problemMessage = 'Week Number must be between 1 and 52.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Contract Number is not blank
            if (record["ContractId"].value == '' ) {
                record["ContractId"].valid = false;
                var problemMessage = 'Contract Number not provided.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

			// Unit exists
            if (record["UnitId"].value == '' ) {
                record["UnitId"].valid = false;
                var problemMessage = 'Unit does not exist.';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

			// Currency fields valid
            if (!isNumeric(record["ContractPrice"].value)){
                record["ContractPrice"].valid = false;
                var problemMessage = 'ContractPrice value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["Financed"].value)){
                record["Financed"].valid = false;
                var problemMessage = 'Financed value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["PreviousLoanTransfer"].value)){
                record["PreviousLoanTransfer"].valid = false;
                var problemMessage = 'PreviousLoanTransfer value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["FundedAmount"].value)){
                record["FundedAmount"].valid = false;
                var problemMessage = 'FundedAmount value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["TransferEquity"].value)){
                record["TransferEquity"].valid = false;
                var problemMessage = 'TransferEquity value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["DownPayment"].value)){
                record["DownPayment"].valid = false;
                var problemMessage = 'DownPayment value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["PercentOfDP"].value)){
                record["PercentOfDP"].valid = false;
                var problemMessage = 'PercentOfDP value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["ClosingCost"].value)){
                record["ClosingCost"].valid = false;
                var problemMessage = 'ClosingCost value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["Interval"].value)){
                record["Interval"].valid = false;
                var problemMessage = 'Interval value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["Term"].value)){
                record["Term"].valid = false;
                var problemMessage = 'Term value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["APR"].value)){
                record["APR"].valid = false;
                var problemMessage = 'APR value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["PayAmount"].value)){
                record["PayAmount"].valid = false;
                var problemMessage = 'PayAmount value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["NetCommissionSalesPrice"].value)){
                record["NetCommissionSalesPrice"].valid = false;
                var problemMessage = 'NetCommissionSalesPrice value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["FdiPointsProvided"].value)){
                record["FdiPointsProvided"].valid = false;
                var problemMessage = 'FdiPointsProvided value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["CommissionDiscountAmount"].value)){
                record["CommissionDiscountAmount"].valid = false;
                var problemMessage = 'CommissionDiscountAmount value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["FdiPointsAuthorized"].value)){
                record["FdiPointsAuthorized"].valid = false;
                var problemMessage = 'FdiPointsAuthorized value wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }


/*
            // Data lengths
            if (record["BatchNumber"].value.length > 10){
                record["BatchNumber"].valid = false;
                var problemMessage = 'BatchNumber longer than 10 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["ResortCode"].value.length > 50){
                record["ResortCode"].valid = false;
                var problemMessage = 'ResortCode longer than 50 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Season"].value.length > 50){
                record["Season"].valid = false;
                var problemMessage = 'Season longer than 50 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["ContractNumber"].value.length > 20){
                record["ContractNumber"].valid = false;
                var problemMessage = 'ContractNumber longer than 20 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["ContractNumber"].value.length > 20){
                record["ContractNumber"].valid = false;
                var problemMessage = 'ContractNumber longer than 20 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["MembershipNumber"].value.length > 18){
                record["MembershipNumber"].valid = false;
                var problemMessage = 'MembershipNumber longer than 18 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Name1"].value.length > 50){
                record["Name1"].valid = false;
                var problemMessage = 'Name1 longer than 50 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Name2"].value.length > 50){
                record["Name2"].valid = false;
                var problemMessage = 'Name2 longer than 50 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Trust"].value.length > 250){
                record["Trust"].valid = false;
                var problemMessage = 'Trust longer than 250 characters';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Currency fields valid
            if (!isNumeric(record["PurchasePrice"].value)){
                record["PurchasePrice"].valid = false;
                var problemMessage = 'PurchasePrice price wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["AmtFinance"].value)){
                record["AmtFinance"].valid = false;
                var problemMessage = 'AmtFinance wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["CloseCost"].value)){
                record["CloseCost"].valid = false;
                var problemMessage = 'CloseCost wrong';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Validate not blank
            if (record["MembershipNumber"].value == ""){
                record["MembershipNumber"].valid = false;
                var problemMessage = 'MembershipNumber is blank';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Name1"].value == ""){
                record["Name1"].valid = false;
                var problemMessage = 'Name1 is blank';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["Address"].value == ""){
                record["Address"].valid = false;
                var problemMessage = 'Address is blank';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["City"].value == ""){
                record["City"].valid = false;
                var problemMessage = 'City is blank';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (record["BatchNumber"].value == ""){
                record["BatchNumber"].valid = false;
                var problemMessage = 'BatchNumber is blank';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }


            // Points are numeric
            if (!isNumeric(record["Upgrd_Pts"].value)){
                record["Upgrd_Pts"].valid = false;
                var problemMessage = 'Upgrd_Pts is not a point value';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["Convrt_Pts"].value)){
                record["Convrt_Pts"].valid = false;
                var problemMessage = 'Convrt Pts is not a point value';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["New_Pts"].value)){
                record["New_Pts"].valid = false;
                var problemMessage = 'New Pts is not a point value';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
            if (!isNumeric(record["SumPts"].value)){
                record["SumPts"].valid = false;
                var problemMessage = 'SumPts is not a point value';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Contract Date is a date
            if (!Date.parse(record["ContractDate"].value)){
                record["ContractDate"].valid = false;
                var problemMessage = 'ContractDate is not a valid date';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Calculate Debits and Credits
            record["TotalDebits"].value =  (Number((record["PurchasePrice"].value).replace(/[$,]+/g,"")) + Number((record["CloseCost"].value).replace(/[$,]+/g,""))).toFixed(2);
            record["TotalCredits"].value =	(Number((record["PurchasePrice"].value).replace(/[$,]+/g,"")) + Number((record["CloseCost"].value).replace(/[$,]+/g,""))).toFixed(2);

            // Validate Contract Number is not duplicate
            if (that.sfContracts[record.ContractNumber.value] != null ) {
                record.ContractNumber.valid = false;
                var problemMessage = 'Contract Number already exists in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            // Lender validate exists in db
            if (that.sfLenders[record.Lender.value] == null ) {
                record.Lender.valid = false;
                var problemMessage = 'Lender does not exist in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }

            //  ResortCode validate exists in db SELECT COUNT(*) FROM Resorts WHERE Id =
            if (that.sfResort[record.ResortCode.value] == null ) {
                record.ResortCode.valid = false;
                var problemMessage = 'Resort does not exist in Salesforce';
                record["Problems"] = setProblemMessage(record, problemMessage);
            }
*/

            record["Verified"] = record["isError"] ? {value: 'false', valid: false} : {value: 'true', valid: true};
			inputData.meta.unverified = record["isError"] ? ++inputData.meta.unverified : inputData.meta.unverified;


        });
    }

	/*
		Funcion to load validated data to Salesforce
	*/
    that.loadData = function() {
        if(inputData == null) {return;}

        $j("#popupLoading").show();
        var dataRows = [];
        var rowId = 0;
        var batchType = 'GrandPacific';
        var batchId = 'GrandPacific|' + Date.parse(new Date());
        console.log('Batch Number:', batchId);
        inputData.records.forEach(function(item) {
            //if(item.isError == false) {
            var rowCounter = ++rowId;
            var detail = new Deeded_Batch_Detail__c();
            detail.Name = batchId + '|' + ('00000' + rowCounter).substr(-5);
            detail.Import_Batch_Identifier__c = batchId;
            detail.Import_Batch_Row__c = rowCounter;

			detail.Problems__c = item.Problems.value;
            detail.Verified__c = item.Verified.value == 'true';

			detail.ResortName__c = item.ResortName.value;
			detail.Contract__c = item.Contract.value;
			detail.ContractId__c = item.ContractId.value;
			detail.SalesPerson__c = item.SalesPerson.value;
			detail.TO__c = item.TO.value;
			detail.QAM__c = item.QAM.value;
			detail.CustomerName1__c = item.CustomerName1.value;
			detail.CustomerName2__c = item.CustomerName2.value;
			detail.Source__c = item.Source.value;
			detail.Lender__c = item.Lender.value;
			detail.ContractPrice__c = item.ContractPrice.value.ToNumber();
			if (!isNaN(Date.parse(item.SalesDate.value))) detail.SalesDate__c = item.SalesDate.value.ToDate();
			detail.Financed__c = item.Financed.value.ToNumber();
			detail.PreviousLoanTransfer__c = item.PreviousLoanTransfer.value.ToNumber();
			detail.FundedAmount__c = item.FundedAmount.value.ToNumber();
			detail.TransferEquity__c = item.TransferEquity.value.ToNumber();
			detail.DownPayment__c = item.DownPayment.value.ToNumber();
			detail.PercentOfDP__c = item.PercentOfDP.value.ToNumber();
			detail.ClosingCost__c = item.ClosingCost.value.ToNumber();
			detail.FICO__c = item.FICO.value;
			detail.Interval__c = item.Interval.value.ToNumber();
			detail.Term__c = item.Term.value.ToNumber();
			detail.APR__c = item.APR.value.ToNumber();
			detail.PayAmount__c = item.PayAmount.value.ToNumber();
			if (!isNaN(Date.parse(item.FirstPayDate.value))) detail.FirstPayDate__c = item.FirstPayDate.value.ToDate();
			detail.NetCommissionSalesPrice__c = item.NetCommissionSalesPrice.value.ToNumber();
			detail.SalesCenter__c = item.SalesCenter.value;
			detail.FdiPointsProvided__c = item.FdiPointsProvided.value.ToNumber();
			detail.tu_unit_number1__c = item.tu_unit_number1.value;
			detail.wk_number1__c = item.wk_number1.value.ToNumber();
			detail.Usage__c = item.Usage.value;
			detail.inventory_usage_type1__c = item.inventory_usage_type1.value;
			detail.Ownership__c = item.Ownership.value;
			detail.CommissionDiscountAmount__c = item.CommissionDiscountAmount.value.ToNumber();
			detail.FdiPointsAuthorized__c = item.FdiPointsAuthorized.value.ToNumber();


            detail.Name_1_Salutation__c = item.Name1Salutation.value;
            detail.Name_1_First__c = item.Name1First.value;
            detail.Name_1_Middle__c = item.Name1Middle.value;
            detail.Name_1_Last__c = item.Name1Last.value;
            detail.Name_1_Suffix__c = item.Name1Suffix.value;

            detail.Name_2_Salutation__c = item.Name2Salutation.value;
            detail.Name_2_First__c = item.Name2First.value;
            detail.Name_2_Middle__c = item.Name2Middle.value;
            detail.Name_2_Last__c = item.Name2Last.value;
            detail.Name_2_Suffix__c = item.Name2Suffix.value;

            dataRows.push(detail);
            //}
        });


        BatchContractsController.loadData(
            dataRows, batchType, batchId, inputData.meta.developerId,
            function(result) {
                console.log('Async loadData result: ', result);

                var bi = $j('<span />');
//                for(var prop in result.errors) {
//                    console.log(prop + '=' + result.errors[prop]);
//                }
                bi.append('<strong>Records created in Salesforce:</strong>');
                bi.append('<br />');
                //bi.append('Membership Number records: ' + result.records['Membership_Number__c']);
                //bi.append('<br />');
                //bi.append('Contract records: ' + result.records['Contract__c']);
                //bi.append('<br />');
                //bi.append('Transaction records: ' + result.records['Transaction__c']);
                //bi.append('<br />');
                //bi.append('Owner records: ' + result.records['Owner__c']);
                //bi.append('<br />');
                //bi.append('Owner List records: ' + result.records['Owner_List__c']);
                //bi.append('<br />');
                //bi.append('<br />');
                $j('#insertResult').html(bi);

                var link = $j('<span />');
                link.append('<strong>Link to this batch record:</strong>');
                link.append('<br />');
                var anchor = $j('<a href="/' + result.records['Points_Batch_Header__c'] + '">Batch Record</a>');
                link.append(anchor);
                $j('#batchRecordLink').html(link);
                $j('#submit-load').hide();

                $j("#popupLoading").hide();

            }
        );
    }



    return that;
}

/*
	Purpose:
	Allow user to paste spreadsheet data into the textbox.
	Parse the pasted data into records in memory
	Validate the records in memory.  (Call Salesforce to retrieve data needed to validate.)
	Display the parsed/validated records.
	Allow the user to then Load the records into Salesforce.
	Note:  Parse button = submit-parse, Load button = submit-load, 
*/
$j = jQuery.noConflict();
var inputData;
var bp = {};  



/*
	On page load the document.ready function will run to initialize the page
	It will attach an event handler to the input textbox such that when the user pastes data in it will act to parse the data

	When the user pastes data onto this page, we call getInputData and getParser from this file, 
	then quicParse() from the Developer-specific js file
*/
$j(document).ready(function($) {
    $j("#popupLoading").show();

    bp = new BatchParse; 
    //$j('#closingDatePicker').val(new Date().toDateInputValue());    

    $j('#inputText').on("input selectionchange propertychange", function(){
			$j("#popupLoading").show();
            getInputData();
			if (inputData.meta.developer != '') {
				getParser();  // assign a specific parser to variable bp
				setButtonEvents();
				bp.quickParse();  // call the quickParse method of the format-specific parser
			}
			$j("#popupLoading").hide();
    });

    $j("#closingDate, #expirationDate, #submit-load, #dataTable, #submit-parse").hide();
    $j("#popupLoading").hide();
});

/*
	Read the pasted spreadsheet data
	Parse the text and determine who the developer is
*/
function getInputData() {
    var txt = $j('#inputText').val();
    inputData = Papa.parse(txt, {delimiter: "",newline: ""});
    inputData.meta.unverified = 0;
	inputData.meta.developer = '';
	if (inputData.data[0].length == 35) inputData.meta.developer = 'Grand Pacific';
	console.log("Input data column count: ", inputData.data[0].length);
	console.log(inputData.meta.developer);
}
/*
	Based on the Developer determine which rules to apply for validation 
*/
function getParser() {
	if (inputData.meta.developer == 'Grand Pacific') { bp = GrandPacificParse(); }
}

/*
  Assign event to buttons  
*/
function setButtonEvents() {
    $j('#submit-parse').click(bp.parseData);
    $j('#ExpirationPickList').change(bp.parseData);
    $j('#closingDatePicker').change(bp.parseData);
    $j('#submit-load').click(bp.loadData);
}

/*
	Base class, inherited by format-specific parser classes
	the displayRecords function is common to all format-specific classes.
*/
function BatchParse() {
    var that = {};
    that.sfContracts = {};
    that.sfResort = {};
	that.sfUnits = {};

    that.displayRecords = function() {
        $j("#dataTable").show();
        var table = $j('<table />');
        var headers = inputData.headers;
        var records = inputData.records;
        if ( headers == null ) return;
        var headerRow = $j('<tr />');
        headers.forEach(function (header) {
            if(header.visible == true) {
                headerRow.append('<th>'+ header.label+'</th>');
            }
        });
        table.append(headerRow);
        if (records != null) {
            records.forEach(function (record, index) {
                if (!record.isError) {
                    row = $j('<tr />');
                } else {
                    row = $j('<tr class="rowError" />');
                }
                headers.forEach(function (header, index) {
                    if(header.visible == true) {
                        if (header.inputColumn != 0) {
                            if (record[header.name].valid == true) {
                                row.append('<td>' + record[header.name].value + '</td>');
                            } else {
                                row.append('<td class="cellError">' + record[header.name].value + '</td>');
                            }
                        } else {
                            if (record[header.name].valid == true) {
                                row.append('<td class="cellCalculated">' + record[header.name].value + '</td>');
                            } else {
                                row.append('<td class="cellError">' + record[header.name].value + '</td>');
                            }
                        }
                    }


                });
                table.append(row);
            });
        }
        $j('#parsedDataTable').html(table);

        // Update UI with restults information
        var bi = $j('<span />');
        bi.append('<strong>Parse Results:</strong>');
        bi.append($j('<br/>'));
        bi.append('Source format: ' + inputData.meta.developer);
        bi.append($j('<br/>'));
        bi.append(inputData.records.length + ' Records');
        bi.append($j('<br/>'));
        if (inputData.meta.unverified != 1) {
            bi.append(inputData.meta.unverified + ' Unverified Records');
        } else {
            bi.append(inputData.meta.unverified + ' Unverified Record');
        }
        $j('#parseResult').html(bi);
        $j("#popupLoading").hide();
    }

    return that;

}




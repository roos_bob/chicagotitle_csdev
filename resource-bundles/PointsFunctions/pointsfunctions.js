$j = jQuery.noConflict();
var pointsToSplit;
var oldContract;
var newContract;
var msg = "Cannot exceed total points available.";

$j(document).ready(function($) {

	oldContract = $j('input[id$="ocPoints"]');
	newContract =  $j('input[id$="ncPoints"]');
	pointsToSplit = $j('span[id$="originalPoints"]').text().ToNumber();
	newContract.val(0);

    newContract.on("blur", function(){
		var oldPoints;
		var newPoints;
		newPoints = newContract.val().ToNumber();
		if (newPoints < 0 ) {
			newPoints = 0;
			alert("New Contract Points cannot be negative.");
		}
		oldPoints = pointsToSplit - newPoints;
		if (oldPoints < 0 ) {
			oldPoints = 0;
			newPoints = pointsToSplit;
			alert("New Contract Points cannot exceed available points.");
		}
		oldContract.val( oldPoints );
		newContract.val(newPoints);	
    });

    oldContract.on("blur", function(){
		var oldPoints;
		var newPoints;
		oldPoints = oldContract.val().ToNumber();
		if (oldPoints < 0 ) {
			oldPoints = 0;
			alert("Original Contract Points cannot be negative.");
		}
		newPoints = pointsToSplit - oldPoints;
		if (newPoints < 0 ) {
			newPoints = 0;
			oldPoints = pointsToSplit;
			alert("Original Contract Points cannot exceed available points.");
		}
		newContract.val( newPoints );
		oldContract.val(oldPoints);	

    });

});


String.prototype.ToNumber = function ToNumber(){
    return Number(this.replace(/[$,]+/g,""));
};






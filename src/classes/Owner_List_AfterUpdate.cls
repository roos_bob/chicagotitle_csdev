public class Owner_List_AfterUpdate extends TriggerHandlerBase   {

    public override void mainEntry(TriggerParameters tp) {
        Owner_List_Helper.updateContractRecord(tp.newList);
		Owner_List_Helper.setCurrentOwners(tp.newList);
    }


}
/*
 * Developer: Bob Roos <bob.roos@getconga.com>
 * Description: 
 *    Provides functions related to Deeded Unit Base actions
 */

public without sharing class Deeded_Unit_Base_Helper {

	/*
		Called by After Insert trigger to set up list of Unit Weeks for the base unit
	*/
	public static void createUnitWeekRecords(List<Deeded_Unit_Base__c> baseUnits) {
		if(!TriggerHelper.doExecute('Deeded_Unit_Base.UnitWeeks')){ return; }


		id batchinstanceid = database.executeBatch(new Deeded_Unit_Week_BatchInsert(baseUnits), 50);


	}

	/*
		Called by After Update trigger to update list of Unit Weeks for the base unit
	*/
	public static void updateUnitWeekRecords(List<Deeded_Unit_Base__c> baseUnits, Map<Id, SObject> oldBaseUnits) {
		if(!TriggerHelper.doExecute('Deeded_Unit_Base.UnitWeeksUpdate')){ return; }

		List<Deeded_Unit_Base__c> units = new List<Deeded_Unit_Base__c>();
		// Check if the Unit Name has changed.  If so, we need to update the Unit Week name.
		// TODO:  do the same if the Week Name changes
		for (Deeded_Unit_Base__c unit : baseUnits) {
			Deeded_Unit_Base__c oldUnit = (Deeded_Unit_Base__c)oldBaseUnits.get(unit.Id);
			if (unit.Name != oldUnit.Name) {
				units.add(unit);
			}
		}

		Map<Id, Map<Id, List<Deeded_Unit_Week__c>>> unitWeekMap = new Map<Id, Map<Id, List<Deeded_Unit_Week__c>>>();
		List<Deeded_Unit_Week__c> unitWeeks;

		unitWeeks = [
			SELECT 
				Id, Name, Deeded_Base_Unit__c, Deeded_Base_Unit__r.Name, WeekId__c, 
				WeekId__r.Name, Deeded_Base_Unit__r.Resort__c 
			FROM Deeded_Unit_Week__c 
			WHERE Deeded_Base_Unit__c in :units
		];
		for (Deeded_Unit_Week__c uw : unitWeeks) {
			uw.Name = uw.Deeded_Base_Unit__r.Name + '-' +  uw.WeekId__r.Name;
		}
		update unitWeeks;


	}

}
public class Transaction_AfterUpdate extends TriggerHandlerBase {
	public override void mainEntry(TriggerParameters tp) {
		System.debug('Transaction_AfterUpdate: ' + tp);
        Transaction_Helper.updateContractTransactionLookups(tp.newList);
    }

}
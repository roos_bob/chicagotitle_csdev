@isTest
private class Contract_Helper_Test {

    @testSetup static void setupTestData(){
		List<Developer__c> developer = (List<Developer__c>)TestingUtility.createSObjectList('Developer__c', 1, false, false);
		insert developer;
        List<Resort__c> resorts = (List<Resort__c>)TestingUtility.createSObjectList('Resort__c', 1, false, false);
		resorts[0].Developer__c = developer[0].Id;
        insert resorts;
        List<Membership_Number__c> membershipNumbers = (List<Membership_Number__c>)TestingUtility.createSObjectList('Membership_Number__c', 1, false, false);
        insert membershipNumbers;
        List<Contract__c> contracts = (List<Contract__c>)TestingUtility.createSObjectList('Contract__c', 1, false, false);
        contracts[0].Membership_Number__c = membershipNumbers[0].Id;
        contracts[0].Resort__c = resorts[0].Id;
        insert contracts;
		List<Transaction_Type__c> transactionTypes = (List<Transaction_Type__c>)TestingUtility.createSObjectList('Transaction_Type__c', 6, false, false);
		transactionTypes[0].Name = 'Issued';
		transactionTypes[0].Ownership__c = true;
		//transactionTypes[0].Developer__c = developer[0].Id;
		transactionTypes[1].Name = 'Cancelled';
		transactionTypes[1].Ownership__c = false;
		transactionTypes[1].Points__c = true;
		//transactionTypes[1].Developer__c = developer[0].Id;
		transactionTypes[2].Name = 'Split';
		transactionTypes[2].Ownership__c = true;
		transactionTypes[2].Points__c = true;
		//transactionTypes[2].Developer__c = developer[0].Id;
		transactionTypes[3].Name = 'Transferred';
		transactionTypes[3].Ownership__c = true;
		transactionTypes[3].Points__c = false;
		//transactionTypes[3].Developer__c = developer[0].Id;
		transactionTypes[4].Name = 'Add/Remove Owner';
		transactionTypes[4].Ownership__c = true;
		transactionTypes[4].Points__c = false;
		//transactionTypes[4].Developer__c = developer[0].Id;
		transactionTypes[5].Name = 'Name Change';
		transactionTypes[5].Ownership__c = true;
		transactionTypes[5].Points__c = false;
		//transactionTypes[5].Developer__c = developer[0].Id;

		insert transactionTypes;
        List<Transaction__c> transactions = (List<Transaction__c>)TestingUtility.createSObjectList('Transaction__c', 1, false, false);
        transactions[0].ContractId__c = contracts[0].Id;
		transactions[0].Transaction_Type__c = transactionTypes[0].Id;
        insert transactions;

        List<Owner__c> owners = (List<Owner__c>)TestingUtility.createSObjectList('Owner__c', 3, false, false);
		integer i = 0;
		for (Owner__c owner : owners) {
			owner.Last_Name__c = 'Test ' + i++;
			owner.First_Name__c = 'Firstname';
		}
        insert owners;
        List<Owner_List__c> ownerLists = new List<Owner_List__c>();
		ownerLists.add(new Owner_List__c());
		ownerLists.add(new Owner_List__c());
		ownerLists.add(new Owner_List__c());
		ownerLists[0].Transaction__c = transactions[0].Id;
		ownerLists[1].Transaction__c = transactions[0].Id;
		ownerLists[2].Transaction__c = transactions[0].Id;

		ownerLists[0].Owner__c = owners[0].Id;
		ownerLists[1].Owner__c = owners[1].Id;
		ownerLists[2].Owner__c = owners[2].Id;
		insert ownerLists;
    }

	@isTest
	private static void testCancelTransaction() {
		Test.startTest();
			Contract__c contract = [select id from contract__c limit 1];
			Contract_Helper.newCancelTransaction(contract.id, Date.today(), 'Test Comment');
			List<Transaction__c> trans = [Select Id, Comments__c from Transaction__c where Transaction_Type__r.Name = 'Cancelled' limit 1];
			System.assert(trans.size() == 1);

		Test.stopTest();
	}

	@isTest
	private static void testSplitTransaction() {
		Test.startTest();
			Contract__c contract = [select id from contract__c limit 1];
			List<Owner__c> ownerList = new List<Owner__c>();
			ownerList.add(new Owner__c(Last_Name__c = 'Test x'));
			Contract_Helper.newSplitTransaction(contract.id, 'M-1234', 'C-1234', Date.today(), 'Test Comment', 200, 400, ownerList);
			List<Transaction__c> trans = [Select Id, Comments__c from Transaction__c where Transaction_Type__r.Name = 'Split' limit 1];
			System.assert(trans.size() == 1);

		Test.stopTest();
	}

	@isTest
	private static void testTransferTransaction() {
		Test.startTest();
			Contract__c contract = [select id from contract__c limit 1];
			List<Owner__c> ownerList = new List<Owner__c>();
			ownerList.add(new Owner__c(Last_Name__c = 'Test x'));
			Contract_Helper.newTransferTransaction(contract.id, Date.today(), 'Test Comment', ownerList);
			List<Transaction__c> trans = [Select Id, Comments__c from Transaction__c where Transaction_Type__r.Name = 'Transferred' limit 1];
			System.assert(trans.size() == 1);

		Test.stopTest();
	}

	@isTest
	private static void testAddRemoveTransaction() {
		Test.startTest();
			Contract__c contract = [select id from contract__c limit 1];
			List<Owner__c> ownerList = new List<Owner__c>();
			ownerList.add(new Owner__c(Last_Name__c = 'Test x'));
			List<wrapOwner> oldOwners = new List<wrapOwner>();
			List<Owner__c> owners = [SELECT Id from owner__c];
			oldOwners.add(new wrapOwner(owners[0]));

			Contract_Helper.newAddRemoveTransaction(contract.id, Date.today(), 'Test Comment', ownerList, oldOwners);
			List<Transaction__c> trans = [Select Id, Comments__c from Transaction__c where Transaction_Type__r.Name = 'Add/Remove Owner' limit 1];
			System.assert(trans.size() == 1);

		Test.stopTest();
	}

		@isTest
	private static void testNameChangeTransaction() {
		Test.startTest();
			Contract__c contract = [select id from contract__c limit 1];
			List<Owner__c> owners = [SELECT Id from owner__c];
			owners[0].First_Name__c = 'Changed';

			Contract_Helper.newNameChangeTransaction(contract.id, Date.today(), 'Test Comment', owners);
			List<Transaction__c> trans = [Select Id, Comments__c from Transaction__c where Transaction_Type__r.Name = 'Name Change' limit 1];
			System.assert(trans.size() == 1);

		Test.stopTest();
	}

	@isTest
	private static void testCancelController() {
		List<Transaction__c> trans = [select Id, Name, Child_ContractId__c, Closing_Date__c, Comments__c, ContractId__c, CreatedById, CreatedDate, Employee__c, Entered__c, IsDeleted, LastModifiedById, LastModifiedDate, OwnerId, Points_Batch_Closing__c, Points_Id__c, SystemModstamp, Transaction_Link__c, Transaction_Name__c, Transaction_Type__c, Updated__c from Transaction__c];
		Test.startTest();
			PageReference pr = Page.PointsContractFunctionCancel;
			//Test.setCurrentPage(pr);
			Test.setCurrentPage(new PageReference('/apex/PointsContractFunctionCancel/?contractId=' + trans[0].ContractId__c) );
			ApexPages.StandardController sc = new ApexPages.StandardController(new Transaction__c());
			PointsContractFunctionCancelController p = new PointsContractFunctionCancelController(sc);
			p.saveAndReturn();
		Test.stopTest();
	}

	@isTest
	private static void testSplitController() {
		List<Transaction__c> trans = [select Id, Name, Child_ContractId__c, Closing_Date__c, Comments__c, ContractId__c, CreatedById, CreatedDate, Employee__c, Entered__c, IsDeleted, LastModifiedById, LastModifiedDate, OwnerId, Points_Batch_Closing__c, Points_Id__c, SystemModstamp, Transaction_Link__c, Transaction_Name__c, Transaction_Type__c, Updated__c from Transaction__c];
		Test.startTest();
			PageReference pr = Page.PointsContractFunctionSplit;
			//Test.setCurrentPage(pr);
			Test.setCurrentPage(new PageReference('/apex/PointsContractFunctionSplit/?contractId=' + trans[0].ContractId__c) );
			ApexPages.StandardController sc = new ApexPages.StandardController(new Transaction__c());
			PointsContractFunctionSplitController p = new PointsContractFunctionSplitController(sc);
			p.addOwnerRow();
			p.HideNewOwnerPage();
			p.addOwnerRow();
			p.SaveNewOwnerPage();
			p.ownerRowNumber = 1;
			p.editOwnerRow();
			p.SaveNewOwnerPage();
			p.ownerRowNumber = 1;
			p.deleteOwnerRow();
			p.saveAndReturn();
		Test.stopTest();
	}

	@isTest
	private static void testTransferControllertransfer() {
		List<Transaction__c> trans = [select Id, Name, Child_ContractId__c, Closing_Date__c, Comments__c, ContractId__c, CreatedById, CreatedDate, Employee__c, Entered__c, IsDeleted, LastModifiedById, LastModifiedDate, OwnerId, Points_Batch_Closing__c, Points_Id__c, SystemModstamp, Transaction_Link__c, Transaction_Name__c, Transaction_Type__c, Updated__c from Transaction__c];
		Test.startTest();
			PageReference pr = Page.PointsContractFunctionSplit;
			//Test.setCurrentPage(pr);
			Test.setCurrentPage(new PageReference('/apex/PointsContractFunctionTransfer/?contractId=' + trans[0].ContractId__c + '&action=transfer') );
			ApexPages.StandardController sc = new ApexPages.StandardController(new Transaction__c());
			PointsContractFunctionTransferController p = new PointsContractFunctionTransferController(sc);

			p.addOwnerRowAction();
			p.HideNewOwnerPageAction();
			p.addOwnerRowAction();
			p.editOwner.Last_Name__c = 'Test';
			p.SaveNewOwnerPageAction();
			p.ownerRowNumber = 1;
			p.editOwnerRowAction();
			p.SaveNewOwnerPageAction();
			p.addOwnerRowAction();
			p.editOwner.Last_Name__c = 'Test2';
			p.SaveNewOwnerPageAction();
			p.deleteOwnerRowAction();

			p.saveAndReturn();
		Test.stopTest();
	}

	@isTest
	private static void testTransferControllerchange() {
		List<Transaction__c> trans = [select Id, Name, Child_ContractId__c, Closing_Date__c, Comments__c, ContractId__c, CreatedById, CreatedDate, Employee__c, Entered__c, IsDeleted, LastModifiedById, LastModifiedDate, OwnerId, Points_Batch_Closing__c, Points_Id__c, SystemModstamp, Transaction_Link__c, Transaction_Name__c, Transaction_Type__c, Updated__c from Transaction__c];
		Test.startTest();
			PageReference pr = Page.PointsContractFunctionSplit;
			//Test.setCurrentPage(pr);
			Test.setCurrentPage(new PageReference('/apex/PointsContractFunctionTransfer/?contractId=' + trans[0].ContractId__c + '&action=change') );
			ApexPages.StandardController sc = new ApexPages.StandardController(new Transaction__c());
			PointsContractFunctionTransferController p = new PointsContractFunctionTransferController(sc);

			p.saveAndReturn();
		Test.stopTest();
	}

	@isTest
	private static void testTransferControlleraddremove() {
		List<Transaction__c> trans = [select Id, Name, Child_ContractId__c, Closing_Date__c, Comments__c, ContractId__c, CreatedById, CreatedDate, Employee__c, Entered__c, IsDeleted, LastModifiedById, LastModifiedDate, OwnerId, Points_Batch_Closing__c, Points_Id__c, SystemModstamp, Transaction_Link__c, Transaction_Name__c, Transaction_Type__c, Updated__c from Transaction__c];
		Test.startTest();
			PageReference pr = Page.PointsContractFunctionSplit;
			//Test.setCurrentPage(pr);
			Test.setCurrentPage(new PageReference('/apex/PointsContractFunctionTransfer/?contractId=' + trans[0].ContractId__c + '&action=addremove') );
			ApexPages.StandardController sc = new ApexPages.StandardController(new Transaction__c());
			PointsContractFunctionTransferController p = new PointsContractFunctionTransferController(sc);

			p.saveAndReturn();
		Test.stopTest();
	}



}
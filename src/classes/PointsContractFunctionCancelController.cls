public class PointsContractFunctionCancelController {
	
	private ApexPages.StandardController stdController;
	private Transaction__c trans;
	public Contract__c contract {get; set;}

	public PointsContractFunctionCancelController(ApexPages.StandardController stdController) {
		this.stdController = stdController;
		this.trans = (Transaction__c)stdController.getRecord();
		trans.ContractId__c = ApexPages.currentPage().getParameters().get('contractId');
		contract = [select id, name, Membership_Number__c, Membership_Number__r.Developer__c, First_Current_Owner__c from Contract__c where id = :trans.ContractId__c];
	}

	public PageReference saveAndReturn()
	{
		//stdController.save();
		Contract_Helper.newCancelTransaction(trans.ContractId__c, trans.Closing_Date__c, trans.Comments__c);
		PageReference parentPage = new PageReference('/' + contract.Id);
		parentPage.setRedirect(true);
		return parentPage;
	}

}
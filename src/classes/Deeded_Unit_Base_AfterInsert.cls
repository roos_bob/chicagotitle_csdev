/*
 * Developer: Bob Roos <bob.roos@getconga.com>
 * Description: 
 *    Deeded_Unit_Base__c After Insert trigger handler
 */

public class Deeded_Unit_Base_AfterInsert extends TriggerHandlerBase   {

    public override void mainEntry(TriggerParameters tp) {
		Deeded_Unit_Base_Helper.createUnitWeekRecords(tp.newList);
    }


}
public class BatchPointsController {
	public string inputType {get; set;}
	public string inputData {get; set;}
	public Date fDate {get; set;}
	public BatchPointsController() {
		fDate = Date.today();
	}

	@RemoteAction
	public static Map<String, List<SObject>> getWelkValidationData(	List<string> contractNumberList, List<String> lenderList, List<String> resortCodeList) {
		truncateTestData('Welk');
		Map<String, List<SObject>> outputMap = new Map<String, List<SObject>>();
		// ResortCodeList - simple lookup of full resort name
		List<Resort__c> resorts = [select Id, Developer__c, Code__c, Name from resort__c where Code__c in :resortCodeList];
		outputMap.put('Resort__c', resorts);
		Id clubId = null;
		for ( Resort__c resort : resorts ) {
			if (resort.Developer__c != null) {
				clubId = resort.Developer__c;
				break;
			}
		}
		// Contract Number list (error if already exists)
		outputMap.put('Contract__c', [select Id, Contract_Number__c from Contract__c where Contract_Number__c in :contractNumberList and Resort__r.Developer__c = :clubId]);
		// Lender list (check that the Lender exists, error if not)
		outputMap.put('Lender__c', [Select Id, Developer__c, Name FROM Lender__c where Name in :lenderList and Developer__c = :clubId]);
		// Transaction Type
		outputMap.put('Transaction_Type__c', [select Id, Name, Developer__c from Transaction_Type__c where Name = 'Issued' and Developer__c = :clubId]);
		// Member Type
		outputMap.put('Member_Type__c', [select Id, Name from Member_Type__c where Name = 'Flex' and Developer__c = :clubId]);

		return outputMap;
	}

	@RemoteAction
	public static Map<String, List<SObject>> getShellValidationData(List<string> contractNumberList, List<String> lenderList, List<String> resortCodeList, List<String> expirationDateList, List<String> memberTypeList, List<String> membershipTypeList ) {

		truncateTestData('Shell');

		Map<String, List<SObject>> outputMap = new Map<String, List<SObject>>();

		// ResortCodeList - simple lookup of full resort name
		List<Resort__c> resorts = [select Id, Developer__c, Code__c, Name from resort__c where Code__c in :resortCodeList];
		outputMap.put('Resort__c', resorts);
		Id clubId = null;
		for ( Resort__c resort : resorts ) {
			if (resort.Developer__c != null) {
				clubId = resort.Developer__c;
				break;
			}
		}
		// Contract Number list (error if already exists)
		outputMap.put('Contract__c', [select Id, Contract_Number__c from Contract__c where Contract_Number__c in :contractNumberList and Resort__r.Developer__c = :clubId]);
		// Lender list (check that the Lender exists, error if not)
		outputMap.put('Lender__c', [Select Id, Name FROM Lender__c where Name in :lenderList and Developer__c = :clubId]);
		// Member Type
		outputMap.put('Member_Type__c', [select Id, Name from Member_Type__c where Name in :memberTypeList and Developer__c = :clubId]);
		// MembershipType
		outputMap.put('Membership_Type__c', [select Id, Name from Membership_Type__c where Name in :membershipTypeList and Developer__c = :clubId]);
		// Expiration Date
		outputMap.put('Expiration_Date__c', [select Id, Name, Date_String__c, Expiration_Date__c from Expiration_Date__c where Date_String__c in :expirationDateList and Developer__c = :clubId]);
		outputMap.put('SelectExpirationDate', [select Date_String__c from Expiration_Date__c where Active__c = true and Developer__c = :clubId order by Expiration_Date__c]);

		// Transaction Type
		outputMap.put('Transaction_Type__c', [select Id, Name, Developer__c from Transaction_Type__c where Name = 'Issued' and Developer__c = :clubId]);

		System.debug(outputMap);
		return outputMap;
}

	@RemoteAction
	public static Map<String, Object> loadData(List<Points_Batch_Closing__c>  dataRows, string batchType, string batchId, string clubId ) {

		Map<String, Object> retMap = new Map<String, Object>();
		retMap.put('errors', new Map<String, Object>() );
		retMap.put('records', new Map<String, Object>() );

		Savepoint sp = Database.setSavepoint();

		Id batchHeaderRecordType;
		Id batchRecordType;

		// Batch Header
		batchHeaderRecordType =  sObjectType.Points_Batch_Header__c.RecordTypeInfosByName.get(batchType).RecordTypeId;
		batchRecordType =  sObjectType.Points_Batch_Closing__c.RecordTypeInfosByName.get(batchType).RecordTypeId;

		Points_Batch_Header__c pbh = new Points_Batch_Header__c();
		pbh.RecordTypeId = batchHeaderRecordType;
		pbh.Name = batchId;
		pbh.Points_Id__c = batchId;
		pbh.Batch_Date__c = Date.today();
		pbh.Employee__c = UserInfo.getUserId();
		pbh.Developer__c = clubId;
		try {
			database.insert(pbh, true);
			((Map<String, Object>)retMap.get('records')).put('Points_Batch_Header__c', pbh.Id);
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Points_Batch_Header__c', e.getMessage() );
			return retMap;
		}

		// Batch Details raw data insert to ARCHIVE table
		for (Points_Batch_Closing__c bc : dataRows) {
			bc.RecordTypeId = batchRecordType;
			bc.Points_Batch_Header__c = pbh.Id;
		}
		try {
			database.insert(dataRows, true);
			((Map<String, Object>)retMap.get('records')).put('Points_Batch_Header__c', pbh.Id);
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Points_Batch_Header__c', e.getMessage() );
			return retMap;
		}

		if(batchType == 'Welk') {  processWelkData(dataRows, batchId, clubId, retMap, sp); }
		if(batchType == 'Shell') {  processShellData(dataRows, batchId, clubId, retMap, sp); }

		return retMap;

	}

	public static void processWelkData(List<Points_Batch_Closing__c>  dataRows, string batchId, string clubId, Map<String, Object> retMap, Savepoint sp ) {
		// Use raw data to insert transactions
		// Create collections of data prior to inserting
		Map<Integer, Membership_Number__c> membershipNumberMap = new Map<Integer, Membership_Number__c>();
		Map<Integer, List<Owner__c>> ownerMap = new Map<Integer, List<Owner__c>>();
		Map<Integer, Contract__c> contractMap = new Map<Integer, Contract__c>();
		Map<Integer, Transaction__c> transactionMap = new Map<Integer, Transaction__c>();
		Map<Integer, List<Owner_List__c>> ownerListMap = new Map<Integer, List<Owner_List__c>>();

		// Populate collections
		integer i = 1;
		Map<String,String> seasonMap = new Map<String,String>{'A' => 'Annual', 'O' => 'Odd', 'E' => 'Even'};
		for (Points_Batch_Closing__c bc : dataRows) {
			// If the row was not validated successfully, do not load the transactions
			if (bc.Verified__c == false) { continue; }
			// Membership Number
			membershipNumberMap.put(i, new Membership_Number__c(Name = bc.Owner__c, Points_Batch_Closing__c = bc.Id, Points_Id__c = bc.Name, Developer__c = clubId ));

			// Owners : There can be more than one owner per incoming record
			List<Owner__c> owners = new List<Owner__c>();
			List<Owner_List__c> ownerLists = new List<Owner_List__c>();
			for(integer nameIterator=1; nameIterator<=2; nameIterator++ ) {
				string ownerName = String.valueOf(bc.get('Name_' + nameIterator + '__c'));
				if(ownerName != '') {
					Owner__c c = new Owner__c();
					c.Points_Batch_Closing__c = bc.Id;
					c.Points_Id__c = bc.Name + '|' + nameIterator;
					c.Salutation__c = String.valueOf(bc.get('Name_' + nameIterator + '_Salutation__c'));
					c.First_Name__c = String.valueOf(bc.get('Name_' + nameIterator + '_First__c'));
					c.Middle_Name__c = String.valueOf(bc.get('Name_' + nameIterator + '_Middle__c'));
					c.Last_Name__c = String.valueOf(bc.get('Name_' + nameIterator + '_Last__c'));
					c.Suffix__c = String.valueOf(bc.get('Name_' + nameIterator + '_Suffix__c'));
					c.Street__c = bc.Owner_Address__c;
					c.City__c = bc.Owner_City__c;
					c.State__c = bc.Owner_State__c;
					c.Postal_Code__c = bc.Owner_Postal_Code__c;
					c.Country__c = bc.Owner_Country__c;
					c.Trust__c = bc.Trust__c == '' ? null : bc.Trust__c;
					owners.add(c);

					Owner_List__c ol = new Owner_List__c();
					ol.Points_Id__c = bc.Name + '|' + nameIterator;
					ol.Points_Batch_Closing__c = bc.Id;
					ownerLists.add(ol);
				}
			}
			ownerMap.put(i, owners);
			ownerListMap.put(i, ownerLists);



			// Contract
			Contract__c contract = new Contract__c();
			contract.Points_Batch_Closing__c = bc.Id;
			contract.Points_Id__c = bc.Name;
			contract.Contract_Number__c = bc.Contract__c;
			contract.Date_Signed__c = bc.Contract_Date__c;
			contract.Name = bc.Contract__c;
			contract.Points__c = bc.Sum_Points__c;
			if (bc.Season__c == 'A' || bc.Season__c == 'O' || bc.Season__c == 'E') {
				contract.Season__c = seasonMap.get(bc.Season__c);
			}
			contract.Batch_Number__c = String.valueOf(bc.Batch__c);


			contract.Resort__c = bc.Resort__c;
			contract.Lender_Id__c = bc.Lender_Id__c;
			contract.Member_Type__c = bc.Member_Type_Id__c;
			contractMap.put(i, contract);

			// Transaction
			Transaction__c trans = new Transaction__c();
			trans.Points_Batch_Closing__c = bc.Id;
			trans.Points_Id__c = bc.Name;

			trans.Transaction_Type__c =  bc.Transaction_Type_Id__c;
			trans.Closing_Date__c = bc.Closing_Date__c;  // closing date from picklist
			trans.Entered__c = Date.today();
			trans.Employee__c = UserInfo.getUserId();
			transactionMap.put(i, trans);

			i++;
		}
		insertTransactions(membershipNumberMap, ownerMap, contractMap, transactionMap, ownerListMap, retMap, sp);
	}

	public static void processShellData(List<Points_Batch_Closing__c>  dataRows, string batchId, string clubId, Map<String, Object> retMap, Savepoint sp ) {
		// Use raw data to insert transactions
		// Create collections of data prior to inserting
		Map<Integer, Membership_Number__c> membershipNumberMap = new Map<Integer, Membership_Number__c>();
		Map<Integer, List<Owner__c>> ownerMap = new Map<Integer, List<Owner__c>>();
		Map<Integer, Contract__c> contractMap = new Map<Integer, Contract__c>();
		Map<Integer, Transaction__c> transactionMap = new Map<Integer, Transaction__c>();
		Map<Integer, List<Owner_List__c>> ownerListMap = new Map<Integer, List<Owner_List__c>>();


		// Populate collections - Not loading to tables yet in this section

		integer i = 1;
		//Map<String,String> seasonMap = new Map<String,String>{'A' => 'Annual', 'O' => 'Odd', 'E' => 'Even'};
		for (Points_Batch_Closing__c bc : dataRows) {
			if (bc.Verified__c == false) { continue; }
			// Membership Number
			membershipNumberMap.put(i, new Membership_Number__c(Name = bc.Owner__c, Points_Batch_Closing__c = bc.Id, Points_Id__c = bc.Name, Developer__c = clubId ));
			System.debug( new Membership_Number__c(Name = bc.Owner__c, Points_Batch_Closing__c = bc.Id, Points_Id__c = bc.Name, Developer__c = clubId ));
			// Owners : There can be more than one owner per incoming record
			List<Owner__c> owners = new List<Owner__c>();
			List<Owner_List__c> ownerLists = new List<Owner_List__c>();
			for(integer nameIterator=1; nameIterator<=2; nameIterator++ ) {
				string ownerName = String.valueOf(bc.get('Name_' + nameIterator + '__c'));
				if(ownerName != '') {
					Owner__c c = new Owner__c();
					c.Points_Batch_Closing__c = bc.Id;
					c.Points_Id__c = bc.Name + '|' + nameIterator;
					c.First_Name__c = String.valueOf(bc.get('Name_' + nameIterator + '_First__c'));
					c.Last_Name__c = String.valueOf(bc.get('Name_' + nameIterator + '_Last__c'));
					c.Street__c = bc.Owner_Address__c;
					c.City__c = bc.Owner_City__c;
					c.State__c = bc.Owner_State__c;
					c.Postal_Code__c = bc.Owner_Postal_Code__c;
					c.Country__c = bc.Owner_Country__c;
					c.Trust__c = bc.Trust__c == '' ? null : bc.Trust__c;
					owners.add(c);

					Owner_List__c ownerList = new Owner_List__c();
					ownerList.Points_Id__c = bc.Name + '|' + nameIterator;
					ownerList.Points_Batch_Closing__c = bc.Id;
					ownerLists.add(ownerList);
				}
			}
			ownerMap.put(i, owners);
			ownerListMap.put(i, ownerLists);

			// Contract
			Contract__c contract = new Contract__c();
			contract.Points_Batch_Closing__c = bc.Id;
			contract.Points_Id__c = bc.Name;
			contract.Name = bc.Contract__c;
			contract.Contract_Number__c = bc.Contract__c;
			contract.Points__c = bc.Convert_Points__c +	bc.New_Points__c;

			contract.Date_Signed__c = bc.Contract_Date__c;

			contract.Batch_Number__c = String.valueOf(bc.Batch__c);
			contract.Resort__c = bc.Resort__c;
			contract.Lender_Id__c = bc.Lender_Id__c;
			contract.Expiration_Date__c = bc.Expiration_Date_Id__c;
			contract.Membership_Type__c = bc.Membership_Type_Id__c;
			contract.Member_Type__c = bc.Member_Type_Id__c;

			contractMap.put(i, contract);

			// Transaction
			Transaction__c trans = new Transaction__c();
			trans.Points_Batch_Closing__c = bc.Id;
			trans.Points_Id__c = bc.Name;
			trans.Closing_Date__c = bc.Closing_Date__c;
			trans.Entered__c = Date.today();
			trans.Transaction_Type__c = bc.Transaction_Type_Id__c;
			trans.Employee__c = UserInfo.getUserId();
			transactionMap.put(i, trans);

			i++;
		}

		insertTransactions(membershipNumberMap, ownerMap, contractMap, transactionMap, ownerListMap, retMap, sp);

	}

	public static void insertTransactions(
			Map<Integer, Membership_Number__c> membershipNumberMap,
			Map<Integer, List<Owner__c>> ownerMap,
			Map<Integer, Contract__c> contractMap,
			Map<Integer, Transaction__c> transactionMap,
			Map<Integer, List<Owner_List__c>> ownerListMap,
			Map<String, Object> retMap, Savepoint sp) {
		// 1.  Pull out the Membership Numbers from the input paramater
		// 2.  Query for existing Membership Numbers
		// 3.  Update the input parameter with the Id of existing Membership Numbers
		// 4.  Upsert the Membership Numbers
		Map<integer, string> mnMap = new map<integer, string>();
		for(integer key : membershipNumberMap.keySet()) {
			mnMap.put(key, membershipNumberMap.get(key).Name);
		}
		// Query the existing Membership Numbers from SF
		List<Membership_Number__c> mnExisting = [
			select  Id, Name
			from Membership_Number__c
			WHERE Name in :mnMap.values()
		];
		// Build a map of the Membership Number : Id
		Map<string, Id> mnExistingMap = new Map<string, Id>();
		for (Membership_Number__c mn : mnExisting) {
			mnExistingMap.put(mn.Name, mn.Id);
		}
		// Update the incoming Parameter with Id where it exists
		for(integer key : membershipNumberMap.keySet()) {
			string mnName = membershipNumberMap.get(key).Name;
			if(mnExistingMap.get(mnName) != null  ){
				membershipNumberMap.get(key).Id = mnExistingMap.get(mnName);
			}
		}

		// Perform inserts from collections
		// Insert Membership number if new -- no FK fields need to be populated
		try {
			Database.UpsertResult[] srMembershipNumbers = Database.upsert(membershipNumberMap.values(), true);
			debugDatabaseUpsertResult(srMembershipNumbers, 'Membership_Number__c');
			((Map<String, Object>)retMap.get('records')).put('Membership_Number__c', membershipNumberMap.size());
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Membership_Number__c', e.getMessage() );
			System.debug('loadData: ' + retMap);
			return;
		}

		// Insert new Owners (Contact) -- no FK fields need to be populated
		List<Owner__c> combinedOwners = new List<Owner__c>();
		for (integer key : ownerMap.keySet()) {
			combinedOwners.addAll(ownerMap.get(key));
		}

		try {
			System.debug('combinedOwners ' + combinedOwners);
			Database.SaveResult[] srOwners = Database.insert(combinedOwners, true);
			debugDatabaseResult(srOwners, 'Owner');
			((Map<String, Object>)retMap.get('records')).put('Owner__c', combinedOwners.size());
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Owner__c', e.getMessage() );
			return;
		}


		// Insert Contracts -- need to set FK of Membership Number
		for (integer key : contractMap.keySet()) {
			contractMap.get(key).Membership_Number__c = membershipNumberMap.get(key).Id;
		}

		try {
			Database.SaveResult[] srContracts = Database.insert(contractMap.values(), true);
			debugDatabaseResult(srContracts, 'Contract__c');
			((Map<String, Object>)retMap.get('records')).put('Contract__c', contractMap.size());
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Contract__c', e.getMessage() );
			System.debug('Insert Contracts 370: ' + retMap);
			return;
		}

		// Insert Transaction
		for (integer key : transactionMap.keySet()) {
			transactionMap.get(key).ContractId__c = contractMap.get(key).Id;
		}

		try {
			Database.SaveResult[] srTransactions = Database.insert(transactionMap.values(), true);
			debugDatabaseResult(srTransactions, 'Transaction__c');
			((Map<String, Object>)retMap.get('records')).put('Transaction__c', transactionMap.size());
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Transaction__c', e.getMessage() );
			return;
		}

		// Insert Owner List
		List<Owner_List__c> combinedOwnerList = new List<Owner_List__c>();
		for (integer key : ownerListMap.keySet()) {
			for (Owner_List__c item : ownerListMap.get(key)) {
				for (Owner__c owner : ownerMap.get(key)) {
					if (owner.Points_Id__c == item.Points_Id__c) {
						item.Owner__c = owner.Id;
					}
				}
				item.Transaction__c = transactionMap.get(key).Id;
				combinedOwnerList.add(item);
			}
		}
		try {
			Database.SaveResult[] srOwnerList = Database.insert(combinedOwnerList, true);
			debugDatabaseResult(srOwnerList, 'Owner_List__c');
			((Map<String, Object>)retMap.get('records')).put('Owner_List__c', combinedOwnerList.size());
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Owner_List__c', e.getMessage() );
			return;
		}

	}


	public static void debugDatabaseResult(Database.SaveResult[] saveResults, string objectName) {
		for (Database.SaveResult sr : saveResults) {
			if (sr.isSuccess()) {
				// Operation was successful, so get the ID of the record that was processed
				System.debug('Successfully inserted ' + objectName + '. Id: ' + sr.getId());
			}
			else {
				// Operation failed, so get all errors
				for(Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug(objectName + ' fields that affected this error: ' + err.getFields());
				}
			}
		}

	}
	public static void debugDatabaseUpsertResult(Database.UpsertResult[] saveResults, string objectName) {
		for (Database.UpsertResult sr : saveResults) {
			if (sr.isSuccess()) {
				// Operation was successful, so get the ID of the record that was processed
				System.debug('Successfully inserted ' + objectName + '. Id: ' + sr.getId());
			}
			else {
				// Operation failed, so get all errors
				for(Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug(objectName + ' fields that affected this error: ' + err.getFields());
				}
			}
		}

	}


	public static void truncateTestData(string batchType) {
		string sheet = batchType == 'Welk' ? 'WELK|%' :'SHELL|%';
		delete [select Id from Points_Batch_Header__c WHERE Points_Id__c like :sheet ] ;
		delete [select Id from Points_Batch_Closing__c WHERE Import_Batch_Identifier__c like :sheet ] ;
		delete [select id from Membership_Number__c where Points_Id__c like :sheet ] ;
		delete [select Id from Owner__c where Points_Id__c like :sheet ] ;
		delete [select id from Contract__c where Points_Id__c like :sheet ] ;
		delete [select id from Transaction__c where Points_Id__c like :sheet ] ;
		delete [select Id from Owner_List__c where Points_Id__c like :sheet ] ;
		
		
	}

}
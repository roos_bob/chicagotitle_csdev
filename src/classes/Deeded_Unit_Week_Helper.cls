/*
 * Developer: Bob Roos <bob.roos@getconga.com>
 * Description: 
 *    Provides functions related to Deeded Unit Week actions
 */

public without sharing class Deeded_Unit_Week_Helper {

	/*
		Called by After Update trigger to set update the Active and Inactive Contracts
		
	*/
	public static void updateContractRecord(Map<Id, sObject> newMap) {
		if(!TriggerHelper.doExecute('Deeded_Unit_Week.updateContract')){ return; }

		List<Deeded_Unit_Week__c> uws = [
			SELECT
				Deeded_Base_Unit__c, Even_A__c, Even_B__c, Odd_A__c, Odd_B__c, Legacy_Id__c, 
				(select id, name, Active_Unit_WeekId__c, Inactive_Unit_WeekId__c from Deeded_Contracts__r) 
			FROM deeded_Unit_week__c
			WHERE id in :newMap.keySet()
		];
		List<Deeded_Contract__c> contracts = new List<Deeded_Contract__c>();
		for (Deeded_Unit_Week__c uw : uws) { 
			Set<Id> activeContracts = new Set<Id>();
			activeContracts.add(uw.Even_A__c);
			activeContracts.add(uw.Even_B__c);
			activeContracts.add(uw.Odd_A__c);
			activeContracts.add(uw.Odd_B__c);
			for (Deeded_Contract__c c : uw.Deeded_Contracts__r) {
				if (activeContracts.contains(c.Id)) {
					c.Active_Unit_WeekId__c = uw.Id;
					c.Inactive_Unit_WeekId__c = null;
				}  else {
					c.Active_Unit_WeekId__c = null;
					c.Inactive_Unit_WeekId__c = uw.Id;
				}
				contracts.add(c);
			}
		}
		update contracts;
	}
}
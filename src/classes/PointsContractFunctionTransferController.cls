public class PointsContractFunctionTransferController {
	private ApexPages.StandardController stdController;

	public List<Transaction__c> originalTransactions {get; set;}
	public List<Owner__c> newOwnerList {get; set;}
	public List<wrapOwner> oldOwnerList {get; set;}

	public Transaction__c newTrans {get; set;}
	public Owner__c editOwner {get; set;}
	public Contract__c originalContract {get; set;}

	private Id originalContractId;
	public Id lastOwnerTransId {get; set;}
	public Boolean ShowOwnerPopup {get; set;}
	public Boolean isCreateOwner {get; set;}
	public Boolean isEditOwner {get; set;}
	public Boolean isTransfer {get; set;}
	public Boolean isChange {get; set;}
	public Boolean isAddRemove {get; set;}
	public integer ownerRowNumber {get; set;}
	public string pageAction {get; set;}
	public string pageTitle {get; set;}


	/*Constructor*/
	public PointsContractFunctionTransferController(ApexPages.StandardController stdController) {
		this.stdController = stdController;   
		this.newTrans = (Transaction__c)stdController.getRecord();
		originalContractId = ApexPages.currentPage().getParameters().get('contractId');
		// pageAction can be one of : transfer, addremove, change
		pageAction = ApexPages.currentPage().getParameters().get('action');
		isTransfer = pageAction == 'transfer';
		isChange = pageAction == 'change';
		isAddRemove =  pageAction == 'addremove';
		newOwnerList = new List<Owner__c>();
		newTrans.ContractId__c = originalContractId;
		ShowOwnerPopup = false;
		isCreateOwner = false;
		isEditOwner = false;

		originalContract = Contract_Helper.getContract(originalContractId);
		lastOwnerTransId = originalContract.Last_Owner_Trans__c;

		List<Owner_List__c> ol = Contract_Helper.getOwnerList(lastOwnerTransId);
		List<Owner__c> owners = Contract_Helper.getOwners(lastOwnerTransId);

		 // TRANSFER:  Remove all current owners, add new owners
		if (isTransfer) {
			//Show Contract context
			//Show Date and Comment prompts

			//Show New Owners grid
			pageTitle = 'Transfer Ownership';

		}
		// ADDREMOVE: Keep at least one owner, add/remove others
		if (isAddRemove) {
			//Show Contract context
			//Show Date and Comment prompts

			//Show checkbox list of current owners ("REMOVE", remove selected items)
			//Show New Owners grid
			pageTitle = 'Add Remove Owners';
			oldOwnerList = new List<wrapOwner>();
			for (Owner__c owner : owners) {
				oldOwnerList.add(new wrapOwner(owner));
			}
			//newOwnerList  is an empty list

		}
		// CHANGE: Keep all owners, change name(s)
		if (isChange) {
			//Show Contract context
			//Show Date and Comment prompts
			//Hide New Owners grid
			// Show editable list of all owners
			newOwnerList = owners;
			pageTitle = 'Owner Name Change';
		}

	}

	public void	addOwnerRowAction() {
		isCreateOwner = true;
		Owner__c o = new Owner__c();
		o.Developer__c = originalContract.Membership_Number__r.Developer__c;
		newOwnerList.add(o);
		editOwner = o;
		ShowOwnerPopup = true;
	}

	public void editOwnerRowAction() {
		isEditOwner = true;
		ShowOwnerPopup = true;
		editOwner = newOwnerlist.get(ownerRowNumber-1);
	}
	public void deleteOwnerRowAction() {
		ShowOwnerPopup = false;
		newOwnerList.remove(ownerRowNumber-1);
	}

	public void SaveNewOwnerPageAction() {
		ShowOwnerPopup = false;
		isCreateOwner = false;
		isEditOwner = false;
	}
	public void HideNewOwnerPageAction() {
		ShowOwnerPopup = false;
		if (isCreateOwner == true) {
			Integer sz = newOwnerList.size() - 1;
			newOwnerList.remove(sz);
		}
		isCreateOwner = false;
		isEditOwner = false;

	}
	
	public PageReference saveAndReturn()
	{
		//stdController.save();
		if(pageAction == 'transfer') {
			Contract_Helper.newTransferTransaction(originalContractId, newTrans.Closing_Date__c, newTrans.Comments__c, newOwnerList );
		}
		if (pageAction == 'addremove') {
			Contract_Helper.newAddRemoveTransaction(originalContractId, newTrans.Closing_Date__c, newTrans.Comments__c, newOwnerList, oldOwnerList );
		}
		if (pageAction == 'change') {
			Contract_Helper.newNameChangeTransaction(originalContractId, newTrans.Closing_Date__c, newTrans.Comments__c, newOwnerList );
		}
		PageReference parentPage = new PageReference('/' + originalContractId);
		parentPage.setRedirect(true);
		return parentPage;
	}

	//class wrapOwner {
		//public Boolean selected {get; set;}
		//public Owner__c owner {get; set;} 
		//public wrapOwner(Owner__c o) {
			//selected = false;
			//owner = o;	
		//}
	//}



}
public class Owner_List_AfterDelete extends TriggerHandlerBase   {

    public override void mainEntry(TriggerParameters tp) {
		System.debug('Owner_List_AfterDelete: ' + tp);
		Owner_List_Helper.updateContractRecord(tp.oldList);   
		Owner_List_Helper.setCurrentOwners(tp.oldList);
    }


}
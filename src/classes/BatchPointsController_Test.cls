@isTest
private class BatchPointsController_Test {

    @testSetup static void setupTestData(){
		List<Developer__c> developer = (List<Developer__c>)TestingUtility.createSObjectList('Developer__c', 2, false, false);
		developer[0].Name = 'Welk';
		developer[1].Name = 'Shell';
		insert developer;
        List<Resort__c> resorts = (List<Resort__c>)TestingUtility.createSObjectList('Resort__c', 1, false, false);
		resorts[0].Developer__c = developer[0].Id;
		resorts[0].Code__c = 'test';
        insert resorts;
	


	}

	@isTest
	private static void testBatchPointsWelk() {
		Test.startTest();
			PageReference pr = Page.BatchPoints;
			Test.setCurrentPage(pr);
			//Test.setCurrentPage(new PageReference('/apex/PointsContractFunctionTransfer/?contractId=' + trans[0].ContractId__c + '&action=transfer') );
			//ApexPages.StandardController sc = new ApexPages.StandardController(new Transaction__c());
			BatchPointsController p = new BatchPointsController();
			p.inputType = '';
			p.inputData = '';
			p.fDate = Date.today();

			List<string> contractNumberList = new List<string>{'test'};
			List<string> lenderList = new List<string>{'test'};
			List<string> resortCodeList = new List<string>{'test'};
			List<string> expirationDateList = new List<string>{'test'};
			List<string> memberTypeList = new List<string>{'test'};
			List<string> membershipTypeList = new List<string>{'test'};

			BatchPointsController.getWelkValidationData( contractNumberList, lenderList, resortCodeList);
			BatchPointsController.getShellValidationData(contractNumberList, lenderList, resortCodeList, 
				expirationDateList, memberTypeList, membershipTypeList);

			//List<Points_Batch_Closing__c> dataRows = new List<Points_Batch_Closing__c>();
			List<Points_Batch_Closing__c> dataRows = (List<Points_Batch_Closing__c>)TestingUtility.createSObjectList('Points_Batch_Closing__c', 5, false, false);
			integer i = 0;
			for (Points_Batch_Closing__c bc : dataRows) {
				bc.Verified__c = true;
				bc.Name_1_Salutation__c = 'Mr';
				bc.Name_1_First__c = 'first';
				bc.Name_1_Middle__c = '';
				bc.Name_1_Last__c = 'Test' + i++;
				bc.Name_1_Suffix__c = '';
				bc.Trust__c = '';

				bc.Name_2_Salutation__c = 'Mr';
				bc.Name_2_First__c = 'second';
				bc.Name_2_Middle__c = '';
				bc.Name_2_Last__c = 'Test' + i;
				bc.Name_2_Suffix__c = '';
				
				bc.Name = 'Test' + i;
				bc.Owner__c = 'test' + i;

				bc.Resort__c = [select id from Resort__c limit 1][0].Id;
			}
			String batchType = 'Welk';
			String batchId = 'Test';
			List<Developer__c> developer = [Select Id from Developer__c where Name = :batchType];
			string clubid = developer[0].Id;
			BatchPointsController.loadData(dataRows, batchType, batchId, clubId );

		Test.stopTest();
	}

	@isTest
	private static void testBatchPointsShell() {
		Test.startTest();
			PageReference pr = Page.BatchPoints;
			Test.setCurrentPage(pr);
			//Test.setCurrentPage(new PageReference('/apex/PointsContractFunctionTransfer/?contractId=' + trans[0].ContractId__c + '&action=transfer') );
			//ApexPages.StandardController sc = new ApexPages.StandardController(new Transaction__c());
			BatchPointsController p = new BatchPointsController();
			p.inputType = '';
			p.inputData = '';
			p.fDate = Date.today();

			List<string> contractNumberList = new List<string>{'test'};
			List<string> lenderList = new List<string>{'test'};
			List<string> resortCodeList = new List<string>{'test'};
			List<string> expirationDateList = new List<string>{'test'};
			List<string> memberTypeList = new List<string>{'test'};
			List<string> membershipTypeList = new List<string>{'test'};

			BatchPointsController.getWelkValidationData( contractNumberList, lenderList, resortCodeList);
			BatchPointsController.getShellValidationData(contractNumberList, lenderList, resortCodeList, expirationDateList, memberTypeList, membershipTypeList);

			//List<Points_Batch_Closing__c> dataRows = new List<Points_Batch_Closing__c>();
			List<Points_Batch_Closing__c> dataRows = (List<Points_Batch_Closing__c>)TestingUtility.createSObjectList('Points_Batch_Closing__c', 5, false, false);
			integer i = 0;
			for (Points_Batch_Closing__c bc : dataRows) {
				bc.Verified__c = true;
				bc.Name_1_Salutation__c = 'Mr';
				bc.Name_1_First__c = 'first';
				bc.Name_1_Middle__c = '';
				bc.Name_1_Last__c = 'Test' + i;
				bc.Name_1_Suffix__c = '';
				bc.Trust__c = '';
				
				bc.Name_2_Salutation__c = 'Mr';
				bc.Name_2_First__c = 'second';
				bc.Name_2_Middle__c = '';
				bc.Name_2_Last__c = 'Test' + i++;
				bc.Name_2_Suffix__c = '';


				bc.Convert_Points__c = 500;
				bc.New_Points__c = 600;
				bc.Name = 'Test' + i;
				bc.Owner__c = 'test' + i;

				bc.Resort__c = [select id from Resort__c limit 1][0].Id;
			}
			String batchType = 'Shell';
			String batchId = 'Test';
			List<Developer__c> developer = [Select Id from Developer__c where Name = :batchType];
			string clubid = developer[0].Id;
			BatchPointsController.loadData(dataRows, batchType, batchId, clubId );

		Test.stopTest();
	}


}
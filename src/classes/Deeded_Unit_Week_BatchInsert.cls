global class Deeded_Unit_Week_BatchInsert implements Database.Batchable<SObject> {
	
	global List<Deeded_Unit_Base__c> BaseUnits;
	global boolean executeError = false;

	global Deeded_Unit_Week_BatchInsert(List<Deeded_Unit_Base__c> pBaseUnits) {
		BaseUnits = pBaseUnits;
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */ 
	//global Database.QueryLocator start(Database.BatchableContext context) {
		//return Database.getQueryLocator('SELECT Id, Name FROM Deeded_Unit_Week__c');
	//}

	public Iterable<sObject> start(Database.BatchableContext BC) {
		return BaseUnits;
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */ 
   	global void execute(Database.BatchableContext context, List<Deeded_Unit_Base__c> scope) {
		try {
			Set<Id> resortSet = new Set<Id>();
			Map<Id, List<Week__c>> weekMap = new Map<Id, List<Week__c>>();
			List<Deeded_Unit_Week__c> unitWeeks = new List<Deeded_Unit_Week__c>();
			List<Week__c> weeks;
				
			for (Deeded_Unit_Base__c baseUnit : scope) {
				resortSet.add(baseUnit.resort__c);
			}
			weeks = [select Id, Name, Resort__c, Legacy_id__c from Week__c where resort__c in :resortSet order by resort__c, name];
			// Set up a map of ResortIds and Weeks
			for (Week__c week : weeks) {
				if( !weekMap.containsKey(week.Resort__c) ) {
					weekMap.put( week.Resort__c, new List<Week__c>() );
				}
				weekMap.get(week.Resort__c).add(week);
			}

			for (Deeded_Unit_Base__c baseUnit : scope) {
				for (Week__c week : weekMap.get(baseUnit.Resort__c)) {
					Deeded_Unit_Week__c uw = new Deeded_Unit_Week__c();
					uw.Name = baseUnit.Name + '-' + week.Name;
					uw.Deeded_Base_Unit__c = baseUnit.Id;
					uw.WeekId__c = week.Id;
					if (week.Legacy_id__c != null && baseUnit.Name != null) {
						uw.Legacy_Id__c = week.Legacy_id__c + '-' + baseUnit.Name;
					}
					unitWeeks.add(uw);
				}
			}
			insert unitWeeks;

		} catch(Exception exc) {
			executeError = true;
			System_Issue_Log_Helper.LogException(new SystemIssueLogWrapper('Deeded_Unit_Week_BatchInsert', 'execute', new List<Id> {context.getJobId()}, exc ) ,true);
		}
	}
	
	
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		
	}
}
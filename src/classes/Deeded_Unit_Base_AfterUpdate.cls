/*
 * Developer: Bob Roos <bob.roos@getconga.com>
 * Description: 
 *    Deeded_Unit_Base__c After Update trigger handler
 */

public class Deeded_Unit_Base_AfterUpdate extends TriggerHandlerBase   {

    public override void mainEntry(TriggerParameters tp) {
		Deeded_Unit_Base_Helper.updateUnitWeekRecords(tp.newList, tp.oldMap);
    }


}
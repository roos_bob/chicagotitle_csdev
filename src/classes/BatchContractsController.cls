public class BatchContractsController {
	public string inputType {get; set;}
	public string inputData {get; set;}
	public Date fDate {get; set;}
	public BatchContractsController() {
		fDate = Date.today();
	}

	@RemoteAction
	public static Map<String, List<SObject>> getGrandPacificValidationData (	
			List<string> contractNumberList, 
			List<String> resortNameList,
			Map<String, List<String>> unitNumberList
		) {
		Map<String, List<SObject>> outputMap = new Map<String, List<SObject>>();
		// Resort List - simple lookup of full resort name
		List<Resort__c> resorts = [select Id, Name, Developer__c from resort__c where Name in :resortNameList];
		outputMap.put('Resort__c', resorts);
		Id developerId = null;
		for ( Resort__c resort : resorts ) {
			if (resort.Developer__c != null) {
				developerId = resort.Developer__c;
				break;
			}
		}
		// Contract Number list (error if already exists)
		outputMap.put('Deeded_Contract__c', [select Id, Contract_Number__c from Contract__c where Contract_Number__c in :contractNumberList and Resort__r.Developer__c = :developerId]);
		// Unit Number List (error if NOT exists)
		System.debug(unitNumberList);
		for (string resortName : unitNumberList.keySet()) {
			outputMap.put(resortName, 
				[SELECT Id, Name, Base_Unit__c, Base_Unit__r.Name, Unit_Code__c, Resort__c 
				FROM Deeded_Unit__c 
				WHERE Resort__c = :resortName 
					AND Unit_Code__c in :unitNumberList.get(resortName)
				]);
		}

		return outputMap;
	}



	@RemoteAction
	public static Map<String, Object> loadData(List<Deeded_Batch_Detail__c> dataRows, string batchType, string batchId, string developerId ) {
		Map<String, Object> retMap = new Map<String, Object>();
		
		retMap.put('errors', new Map<String, Object>() );
		retMap.put('records', new Map<String, Object>() );
		
		Savepoint sp = Database.setSavepoint();

		Id batchHeaderRecordType;
		Id batchRecordType;

		// Batch Header
		//batchHeaderRecordType =  sObjectType.Deeded_Batch_Header__c.RecordTypeInfosByName.get(batchType).RecordTypeId;
		//batchRecordType =  sObjectType.Deeded_Batch_Detail__c.RecordTypeInfosByName.get(batchType).RecordTypeId;

		Deeded_Batch_Header__c header = new Deeded_Batch_Header__c();
		//pbh.RecordTypeId = batchHeaderRecordType;
		header.Name = batchId;
		header.Batch__c = batchId;
		header.Batch_Date__c = Date.today();
		header.Employee__c = UserInfo.getUserId();
		header.Developer__c = developerId;

		try {
			database.insert(header, true);
			((Map<String, Object>)retMap.get('records')).put('Deeded_Batch_Header__c', header.Id);
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Deeded_Batch_Header__c', e.getMessage() );
			return retMap;
		}
		system.debug('retMap: ' + retmap);
		system.debug('dataRows: ' + dataRows);
		// batch details raw data insert to archive table
		for (deeded_batch_detail__c detail : datarows) {
			//detail.recordtypeid = batchrecordtype;
			detail.deeded_batch_header__c = header.id;
		}
		try {
			database.insert(datarows, true);
			((map<string, object>)retmap.get('records')).put('deeded_batch_header__c', header.id);
		} catch (exception e) {
			database.rollback(sp);
			((map<string, object>)retmap.get('errors')).put('deeded_batch_header__c', e.getmessage() );
			return retmap;
		}

		//if(batchtype == 'welk') {  processwelkdata(datarows, batchid, developerId, retmap, sp); }

		return retmap;

	}

	public static void processWelkData(List<Points_Batch_Closing__c>  dataRows, string batchId, string developerId, Map<String, Object> retMap, Savepoint sp ) {
		// Use raw data to insert transactions
		// Create collections of data prior to inserting
		Map<Integer, Membership_Number__c> membershipNumberMap = new Map<Integer, Membership_Number__c>();
		Map<Integer, List<Owner__c>> ownerMap = new Map<Integer, List<Owner__c>>();
		Map<Integer, Contract__c> contractMap = new Map<Integer, Contract__c>();
		Map<Integer, Transaction__c> transactionMap = new Map<Integer, Transaction__c>();
		Map<Integer, List<Owner_List__c>> ownerListMap = new Map<Integer, List<Owner_List__c>>();

		// Populate collections
		integer i = 1;
		Map<String,String> seasonMap = new Map<String,String>{'A' => 'Annual', 'O' => 'Odd', 'E' => 'Even'};
		for (Points_Batch_Closing__c bc : dataRows) {
			// If the row was not validated successfully, do not load the transactions
			if (bc.Verified__c == false) { continue; }
			// Membership Number
			membershipNumberMap.put(i, new Membership_Number__c(Name = bc.Owner__c, Points_Batch_Closing__c = bc.Id, Points_Id__c = bc.Name, Developer__c = developerId ));

			// Owners : There can be more than one owner per incoming record
			List<Owner__c> owners = new List<Owner__c>();
			List<Owner_List__c> ownerLists = new List<Owner_List__c>();
			for(integer nameIterator=1; nameIterator<=2; nameIterator++ ) {
				string ownerName = String.valueOf(bc.get('Name_' + nameIterator + '__c'));
				if(ownerName != '') {
					Owner__c c = new Owner__c();
					c.Points_Batch_Closing__c = bc.Id;
					c.Points_Id__c = bc.Name + '|' + nameIterator;
					c.Salutation__c = String.valueOf(bc.get('Name_' + nameIterator + '_Salutation__c'));
					c.First_Name__c = String.valueOf(bc.get('Name_' + nameIterator + '_First__c'));
					c.Middle_Name__c = String.valueOf(bc.get('Name_' + nameIterator + '_Middle__c'));
					c.Last_Name__c = String.valueOf(bc.get('Name_' + nameIterator + '_Last__c'));
					c.Suffix__c = String.valueOf(bc.get('Name_' + nameIterator + '_Suffix__c'));
					c.Street__c = bc.Owner_Address__c;
					c.City__c = bc.Owner_City__c;
					c.State__c = bc.Owner_State__c;
					c.Postal_Code__c = bc.Owner_Postal_Code__c;
					c.Country__c = bc.Owner_Country__c;
					c.Trust__c = bc.Trust__c == '' ? null : bc.Trust__c;
					owners.add(c);

					Owner_List__c ol = new Owner_List__c();
					ol.Points_Id__c = bc.Name + '|' + nameIterator;
					ol.Points_Batch_Closing__c = bc.Id;
					ownerLists.add(ol);
				}
			}
			ownerMap.put(i, owners);
			ownerListMap.put(i, ownerLists);



			// Contract
			Contract__c contract = new Contract__c();
			contract.Points_Batch_Closing__c = bc.Id;
			contract.Points_Id__c = bc.Name;
			contract.Contract_Number__c = bc.Contract__c;
			contract.Date_Signed__c = bc.Contract_Date__c;
			contract.Name = bc.Contract__c;
			contract.Points__c = bc.Sum_Points__c;
			if (bc.Season__c == 'A' || bc.Season__c == 'O' || bc.Season__c == 'E') {
				contract.Season__c = seasonMap.get(bc.Season__c);
			}
			contract.Batch_Number__c = String.valueOf(bc.Batch__c);


			contract.Resort__c = bc.Resort__c;
			contract.Lender_Id__c = bc.Lender_Id__c;
			contract.Member_Type__c = bc.Member_Type_Id__c;
			contractMap.put(i, contract);

			// Transaction
			Transaction__c trans = new Transaction__c();
			trans.Points_Batch_Closing__c = bc.Id;
			trans.Points_Id__c = bc.Name;

			trans.Transaction_Type__c =  bc.Transaction_Type_Id__c;
			trans.Closing_Date__c = bc.Closing_Date__c;  // closing date from picklist
			trans.Entered__c = Date.today();
			trans.Employee__c = UserInfo.getUserId();
			transactionMap.put(i, trans);

			i++;
		}
		//insertTransactions(membershipNumberMap, ownerMap, contractMap, transactionMap, ownerListMap, retMap, sp);
	}

	public static void insertTransactions(
			Map<Integer, Membership_Number__c> membershipNumberMap,
			Map<Integer, List<Owner__c>> ownerMap,
			Map<Integer, Contract__c> contractMap,
			Map<Integer, Transaction__c> transactionMap,
			Map<Integer, List<Owner_List__c>> ownerListMap,
			Map<String, Object> retMap, Savepoint sp) {
		// 1.  Pull out the Membership Numbers from the input paramater
		// 2.  Query for existing Membership Numbers
		// 3.  Update the input parameter with the Id of existing Membership Numbers
		// 4.  Upsert the Membership Numbers
		Map<integer, string> mnMap = new map<integer, string>();
		for(integer key : membershipNumberMap.keySet()) {
			mnMap.put(key, membershipNumberMap.get(key).Name);
		}
		// Query the existing Membership Numbers from SF
		List<Membership_Number__c> mnExisting = [
			select  Id, Name
			from Membership_Number__c
			WHERE Name in :mnMap.values()
		];
		// Build a map of the Membership Number : Id
		Map<string, Id> mnExistingMap = new Map<string, Id>();
		for (Membership_Number__c mn : mnExisting) {
			mnExistingMap.put(mn.Name, mn.Id);
		}
		// Update the incoming Parameter with Id where it exists
		for(integer key : membershipNumberMap.keySet()) {
			string mnName = membershipNumberMap.get(key).Name;
			if(mnExistingMap.get(mnName) != null  ){
				membershipNumberMap.get(key).Id = mnExistingMap.get(mnName);
			}
		}

		// Perform inserts from collections
		// Insert Membership number if new -- no FK fields need to be populated
		try {
			Database.UpsertResult[] srMembershipNumbers = Database.upsert(membershipNumberMap.values(), true);
			debugDatabaseUpsertResult(srMembershipNumbers, 'Membership_Number__c');
			((Map<String, Object>)retMap.get('records')).put('Membership_Number__c', membershipNumberMap.size());
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Membership_Number__c', e.getMessage() );
			System.debug('loadData: ' + retMap);
			return;
		}

		// Insert new Owners (Contact) -- no FK fields need to be populated
		List<Owner__c> combinedOwners = new List<Owner__c>();
		for (integer key : ownerMap.keySet()) {
			combinedOwners.addAll(ownerMap.get(key));
		}

		try {
			System.debug('combinedOwners ' + combinedOwners);
			Database.SaveResult[] srOwners = Database.insert(combinedOwners, true);
			debugDatabaseResult(srOwners, 'Owner');
			((Map<String, Object>)retMap.get('records')).put('Owner__c', combinedOwners.size());
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Owner__c', e.getMessage() );
			return;
		}


		// Insert Contracts -- need to set FK of Membership Number
		for (integer key : contractMap.keySet()) {
			contractMap.get(key).Membership_Number__c = membershipNumberMap.get(key).Id;
		}

		try {
			Database.SaveResult[] srContracts = Database.insert(contractMap.values(), true);
			debugDatabaseResult(srContracts, 'Contract__c');
			((Map<String, Object>)retMap.get('records')).put('Contract__c', contractMap.size());
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Contract__c', e.getMessage() );
			System.debug('Insert Contracts 370: ' + retMap);
			return;
		}

		// Insert Transaction
		for (integer key : transactionMap.keySet()) {
			transactionMap.get(key).ContractId__c = contractMap.get(key).Id;
		}

		try {
			Database.SaveResult[] srTransactions = Database.insert(transactionMap.values(), true);
			debugDatabaseResult(srTransactions, 'Transaction__c');
			((Map<String, Object>)retMap.get('records')).put('Transaction__c', transactionMap.size());
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Transaction__c', e.getMessage() );
			return;
		}

		// Insert Owner List
		List<Owner_List__c> combinedOwnerList = new List<Owner_List__c>();
		for (integer key : ownerListMap.keySet()) {
			for (Owner_List__c item : ownerListMap.get(key)) {
				for (Owner__c owner : ownerMap.get(key)) {
					if (owner.Points_Id__c == item.Points_Id__c) {
						item.Owner__c = owner.Id;
					}
				}
				item.Transaction__c = transactionMap.get(key).Id;
				combinedOwnerList.add(item);
			}
		}
		try {
			Database.SaveResult[] srOwnerList = Database.insert(combinedOwnerList, true);
			debugDatabaseResult(srOwnerList, 'Owner_List__c');
			((Map<String, Object>)retMap.get('records')).put('Owner_List__c', combinedOwnerList.size());
		} catch (Exception e) {
			Database.rollback(sp);
			((Map<String, Object>)retMap.get('errors')).put('Owner_List__c', e.getMessage() );
			return;
		}

	}


	public static void debugDatabaseResult(Database.SaveResult[] saveResults, string objectName) {
		for (Database.SaveResult sr : saveResults) {
			if (sr.isSuccess()) {
				// Operation was successful, so get the ID of the record that was processed
				System.debug('Successfully inserted ' + objectName + '. Id: ' + sr.getId());
			}
			else {
				// Operation failed, so get all errors
				for(Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug(objectName + ' fields that affected this error: ' + err.getFields());
				}
			}
		}

	}
	public static void debugDatabaseUpsertResult(Database.UpsertResult[] saveResults, string objectName) {
		for (Database.UpsertResult sr : saveResults) {
			if (sr.isSuccess()) {
				// Operation was successful, so get the ID of the record that was processed
				System.debug('Successfully inserted ' + objectName + '. Id: ' + sr.getId());
			}
			else {
				// Operation failed, so get all errors
				for(Database.Error err : sr.getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug(objectName + ' fields that affected this error: ' + err.getFields());
				}
			}
		}

	}


	public static void truncateTestData(string batchType) {
		//string sheet = batchType == 'Welk' ? 'WELK|%' :'SHELL|%';
		//delete [select Id from Points_Batch_Header__c WHERE Points_Id__c like :sheet ] ;
		//delete [select Id from Points_Batch_Closing__c WHERE Import_Batch_Identifier__c like :sheet ] ;
		//delete [select id from Membership_Number__c where Points_Id__c like :sheet ] ;
		//delete [select Id from Owner__c where Points_Id__c like :sheet ] ;
		//delete [select id from Contract__c where Points_Id__c like :sheet ] ;
		//delete [select id from Transaction__c where Points_Id__c like :sheet ] ;
		//delete [select Id from Owner_List__c where Points_Id__c like :sheet ] ;
		//delete [select Id from Contact where Points_Id__c like :sheet ] ;
		
	}

}
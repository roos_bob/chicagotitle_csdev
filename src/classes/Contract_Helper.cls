/*
 * Developer: Bob Roos <bob.roos@getconga.com>
 * Description: 
 *    Provides functions related to Contract actions
 */

public without sharing class Contract_Helper {

	/*
	Cancel function
	Cancel the current contract
	*/
    public static void newCancelTransaction(Id contractId, Date closingDate, string comments ) {
		Contract__c contract = getContract(contractId);
		contract.Points__c = 0;
		Database.update(contract);
		Transaction__c trans = insertNewTransaction('Cancelled', contract, closingDate, comments, 0);
		insertOwnerList(trans.Id, Contract_Helper.getOwners(contract.Last_Owner_Trans__c));
	}

	/*
	Split function
	Take points from one contract and apply them to a newly created contract
	*/
	public static void newSplitTransaction(Id contractId, string membershipNumber, string contractNumber, Date closingDate, String comments, Decimal oldPoints, Decimal newPoints, List<Owner__c> owners) {
		// Original Contract
		Contract__c originalContract = getContract(contractId);
		originalContract.Points__c = oldPoints;
		Database.update(originalContract);
		 
		//New Membership Number
		Id membershipNumberId;
		List<Membership_Number__c> membershipNumbers = [
			select Id 
			from Membership_Number__c 
			WHERE Developer__c = :originalContract.Membership_Number__r.Developer__c 
			AND Name = :membershipNumber
		];
		if (membershipNumbers.size() > 0) {
			membershipNumberId = membershipNumbers[0].Id;
		} else {
			Membership_Number__c mn = new Membership_Number__c();
			mn.Name = membershipNumber;
			mn.Developer__c = originalContract.Membership_Number__r.Developer__c;
			Database.insert(mn);
			membershipNumberId = mn.Id;
		}

		// New Contract (cloned from original contract, linked to new membership number)
		Contract__c newContract = originalContract.clone(false, false, false, false);
		newContract.Membership_Number__c = membershipNumberId;
		newContract.Name = contractNumber;
		newContract.Contract_Number__c = contractNumber;
		newContract.Points__c = newPoints;
		newContract.Batch_Number__c = null;
		newContract.Date_Signed__c = closingDate;
		Database.insert(newContract);


		// New Split Transaction on original contract
		Transaction__c splitTrans = insertNewTransaction('Split', originalContract, closingDate, comments, oldPoints, newPoints, newContract.Id);

		// New "Issued" transaction on new contract
		Transaction__c issuedTrans = insertNewTransaction('Issued', newContract, closingDate, comments, newPoints, null, contractId);

		// New Owners -- these were added on the page by the user
		Database.upsert(owners);

		// New Owner List records
		//Split transaction owner list
		insertOwnerList(splitTrans.Id, Contract_Helper.getOwners(originalContract.Last_Owner_Trans__c));
		// Issued transaction owner list
		insertOwnerList(issuedTrans.Id, owners);

	}
	
	/*
	Transfer function
	On the existing contract, remove ALL current owner, add all new owners		
	*/
	public static void newTransferTransaction(Id contractId, Date closingDate, string comments, List<Owner__c> owners) {
		Contract__c contract = getContract(contractId);
		
		// New Transferred transaction
		Transaction__c newTrans = insertNewTransaction('Transferred', contract, closingDate, comments, contract.Points__c);

		// New Owners
		Database.upsert(owners);

		// New Owner List records
		insertOwnerList(newTrans.Id, owners);
	}

	/*
	Add/Remove function
	Add new owners or remove existing owners to the existing contract
	*/
	public static void newAddRemoveTransaction(Id contractId, Date closingDate, string comments, List<Owner__c> owners, List<wrapOwner> oldOwners) {
		Contract__c contract = getContract(contractId);
		
		Transaction__c newTrans = insertNewTransaction('Add/Remove Owner', contract, closingDate, comments, contract.Points__c);

		// New Owners
		for (wrapOwner oldOwner : oldOwners) {
			if (!oldOwner.selected) owners.add(oldOwner.owner);
		}
		Database.upsert(owners);

		// New Owner List records
		insertOwnerList(newTrans.Id, owners);

	}

	/*
	Name Change function
	Keep all current owners but apply a name change
	*/
	public static void newNameChangeTransaction(Id contractId, Date closingDate, string comments, List<Owner__c> owners) {
		Contract__c contract = getContract(contractId);
		
		Transaction__c newTrans = insertNewTransaction('Name Change', contract, closingDate, comments, contract.Points__c);

		// New Owners
		for (Owner__c owner: owners) {
			owner.Id = null;
		}
		Database.insert(owners);
		
		insertOwnerList(newTrans.Id, owners);
	}

	/*
	Utility method
	Query for the current contract
	*/
	public static Contract__c getContract(Id contractId) {
		Contract__c returnContract = null;
		List<Contract__c> c = [
			select Id, Name, Membership_Number__c, Membership_Number__r.Developer__c, First_Current_Owner__c
				, Batch_Number__c, Contract_Number__c, Date_Signed__c, Expiration_Date__c,  Lender__c 
				, Member_Type__c, Membership_Type__c, Points__c, Resort__c, Season__c
				,Last_Owner_Trans__c
			from Contract__c 
			where id = :contractId
			limit 1
		];
		if (c.size() > 0 ) returnContract = c[0];
		return returnContract;
	}

	/*
	Utility method
	Query for the Transaction Type Id
	*/
	public static Id getTransactionTypeId(string typeName, Id developerId) {
		Id transTypeId = null;
		List<Transaction_Type__c> transTypes = [
			SELECT Id FROM Transaction_Type__c  
			WHERE Name = :typeName 
			AND Developer__c = :developerId
			limit 1
		];
		if (transTypes.size() > 0 ) transTypeId = transTypes[0].Id;
		return transTypeId;
	}

	/*
	Utility method
	Override to create a new Transaction
	*/
	public static Transaction__c insertNewTransaction(string transactionType, Contract__c contract, Date closingDate, string comments, decimal oldPoints) {
		return insertNewTransaction(transactionType, contract, closingDate, comments, oldPoints, null, null);
	}

	/*
	Utility method
	Override to create a new Transaction
	*/	
	public static Transaction__c insertNewTransaction(string transactionType, Contract__c contract, Date closingDate, string comments, decimal oldPoints, decimal newPoints) {
		return insertNewTransaction(transactionType, contract, closingDate, comments, oldPoints, newPoints, null);
	}

	/*
	Utility method
	Create a new Transaction
	*/
	public static Transaction__c insertNewTransaction(string transactionType, Contract__c contract, Date closingDate, string comments, decimal oldPoints, decimal newPoints, Id relatedContractId) {
		Id transactionTypeId = getTransactionTypeId(transactionType, contract.Membership_Number__r.Developer__c);
		Transaction__c trans = new Transaction__c();
		trans.ContractId__c = contract.Id;
		trans.Transaction_Type__c = transactionTypeId;
		trans.Closing_Date__c = closingDate;
		trans.Comments__c = comments;
		trans.Employee__c = UserInfo.getUserId();
		trans.Entered__c = DateTime.now();
		trans.Points__c = oldPoints;
		trans.Split_Points__c = newPoints;
		trans.Child_ContractId__c = relatedContractId;
		Database.insert(trans);
		return trans;
	}

	/*
	Utility method
	Query for related Owner_List__c records
	*/
	public static List<Owner_List__c> getOwnerList(Id transactionId) {
		List<Owner_List__c> ol = [
			SELECT id, name, Owner__c, Transaction__c FROM Owner_List__c WHERE transaction__c = :transactionId
		];
		return ol;
	}

	/*
	Utility method
	Query for related Owner__c records
	*/
	public static List<Owner__c> getOwners(Id transactionId) {
		List<Owner__c> owners = [
			SELECT Id, Name, Salutation__c, First_Name__c, Middle_Name__c, Last_Name__c, Suffix__c, Trust__c, Street__c, City__c, State__c, Country__c, Postal_Code__c, Email__c, Phone__c, Developer__c  
			FROM Owner__c
			WHERE Id in (SELECT Owner__c FROM Owner_List__c	WHERE transaction__c = :transactionId )
		];
		return owners;
	}

	/*
	Utility method
	Save Owner List for transaction
	*/
	public static List<Owner_List__c> insertOwnerList(Id transactionId, List<Owner__c> owners) {
		List<Owner_List__c> ownerLists = new List<Owner_List__c>();
		for (Owner__c owner : owners ) {
			Owner_List__c ol = new Owner_List__c();
			ol.Owner__c = owner.Id;
			ol.Transaction__c = transactionId;
			ownerLists.add(ol);
		}
		Database.insert(ownerLists);
		return ownerLists;
	}

	public static void setCurrentOwners(Set<Id> contractIdList) {
		Map<Id, Contract__c> contracts = new Map<Id, Contract__c>([select Id, Name, Last_Owner_Trans__c from Contract__c where id in :contractIdList]);
		if (contracts.size() == 0 ) return;
		List<Owner_List__c> ownerlist = [select Id, Name, transaction__c, Contract__c, Current_Owner_of_Contract__c from owner_List__c where contract__c in :contractIdList];
		if (ownerlist.size() == 0 ) return;
		for (Owner_List__c ol : ownerlist ) {
			if ( ol.Transaction__c == contracts.get(ol.Contract__c).Last_Owner_Trans__c ) {
				ol.Current_Owner_of_Contract__c = ol.Contract__c;
			} else {
				ol.Current_Owner_of_Contract__c = null;
			}
		}
		Database.update(ownerlist);

	}

}
/**
 * Created by bob.roos on 4/20/2017.
 */

trigger Deeded_Unit_WeekTrigger on Deeded_Unit_Week__c (
        before insert,
        before update,
        before delete,
        after insert,
        after update,
        after delete,
        after undelete) {
    TriggerFactory.createTriggerDispatcher( Deeded_Unit_Week__c.sObjectType);
}
/*
 * Developer: Bob Roos <bob.roos@getconga.com>
 * Description: 
 *    Deeded_Unit_Base__c triggers
 */

trigger Deeded_Unit_BaseTrigger on Deeded_Unit_Base__c (
        before insert,
        before update,
        before delete,
        after insert,
        after update,
        after delete,
        after undelete) {
    TriggerFactory.createTriggerDispatcher( Deeded_Unit_Base__c.sObjectType);
}